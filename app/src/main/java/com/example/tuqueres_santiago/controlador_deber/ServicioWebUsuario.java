package com.example.tuqueres_santiago.controlador_deber;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.tuqueres_santiago.modelo.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServicioWebUsuario {
    String host= "http://192.168.1.36:80/tuqueres_santiago/";
    String get_lista = "wsJSONConsultarLista.php";
    String getByDoc = "wsJSONConsultarUsuario.php?documento=";
    String registro="wsJSONRegistro.php?documento=";
    String registro1="wsJSONRegistroMovil.php";
    String delete = "wsJSONDeleteMovil.php?documento=";
    String update="wsJSONUpdateMovil.php";

    String consulta;

    Context context;

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }
    public ServicioWebUsuario(Context context) {
        this.context = context;
    }

    public String findAll(final  VolleyResponseListener volleyResponseListener){
        String path = host.concat(get_lista);
        JsonObjectRequest request =  new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               // Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                //consulta=response;
                volleyResponseListener.onResponse(response);
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error","error"+error);
            }
        });
        UsuarioSingletonVolley.getInstance(context).addToRequestque(request);
        return consulta;
    }
    public String findByDoc(String doc){
        String path = host.concat(getByDoc)+doc;
        StringRequest request =  new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                consulta=response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error","error");
            }
        });
        UsuarioSingletonVolley.getInstance(context).addToRequestque(request);
        return consulta;
    }
    //
    public JSONObject insertarUser( Usuario alumno){
        String path = host.concat(registro)+alumno.getDocumento()+"&nombre="+alumno.getNombre()+"&profesion="+alumno.getProfesion();

        JSONObject json = new JSONObject();

        try {
            json.put("documento",alumno.getDocumento());
            json.put("nombre",alumno.getNombre());
            json.put("profesion",alumno.getProfesion());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response+"");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Err","error"+error);
            }
        });
        UsuarioSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }
    //Prueba de mandar params
    public void insertarUserPrueba2(final Usuario alumno){
        String path = host.concat(registro1);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(ServicioWebUsuario.this,response,Toast.LENGTH_LONG).show();
                       // parseData(response);
                        Log.e("sucess",response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    Log.e("error",""+error);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("documento",alumno.getDocumento()+"");
                params.put("profesion",alumno.getProfesion());
                params.put("nombre",alumno.getNombre());

                return params;
            }

        };

        //RequestQueue requestQueue = Volley.newRequestQueue(this);
        //requestQueue.add(stringRequest);
        UsuarioSingletonVolley.getInstance(context).addToRequestque(stringRequest);
    }
    public JSONObject eliminarUsario(Usuario alumno){
        String path = host.concat(delete)+alumno.getDocumento();
        JSONObject json = new JSONObject();

        try {
            json.put("documento",alumno.getDocumento());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response+"");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Err","error"+error);
            }
        });
        UsuarioSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }


    public void updateUser(final Usuario alumno){
        String path = host.concat(update);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, path,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(ServicioWebUsuario.this,response,Toast.LENGTH_LONG).show();
                        // parseData(response);
                        Log.e("sucess",response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        Log.e("error",""+error);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("documento",alumno.getDocumento()+"");
                params.put("profesion",alumno.getProfesion());
                params.put("nombre",alumno.getNombre());

                return params;
            }

        };

        //RequestQueue requestQueue = Volley.newRequestQueue(this);
        //requestQueue.add(stringRequest);
        UsuarioSingletonVolley.getInstance(context).addToRequestque(stringRequest);
        Log.e("d}sr",stringRequest+"");
    }
}
