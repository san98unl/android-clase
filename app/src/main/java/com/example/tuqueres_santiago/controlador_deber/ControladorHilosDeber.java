package com.example.tuqueres_santiago.controlador_deber;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.tuqueres_santiago.modelo.Alumno;
import com.example.tuqueres_santiago.modelo.Usuario;
import com.example.tuqueres_santiago.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ControladorHilosDeber  extends AsyncTask<String,Void,String> {
    String post_data;

    Context context;

    List<Usuario> lista;
    //AlertDialog alertDialog;

    public ControladorHilosDeber(Context context) {
        this.context = context;
    }


    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0];//esta es la ruta... http://00rene.../obtener_alumnos.php
        if (parametros[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();


                if (codigo_respuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();

Log.e("res",consulta);





                }
                connection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if (parametros[1].equals("2")){
            //vamos a establecer aqui

            try {
                url = new URL(ruta);
                //se puede utilizar UrlConection y HttpURLConnection para la conexion
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();
                JSONObject json = new JSONObject();
                //para enviar datos se usa el metodo put
                //para obtener los datos se usa la opcion Parametoros


                //buffer establecer la via de comunicacion
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                Log.e("daots", "guardo: " + json.toString());
                //ENvio de datos
                escritor.write(json.toString());
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                   // Toast.makeText(ActividadSWAlumnos.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();

                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(parametros[1].equals("3")){
            try {
                url=new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();

                if (codigo_respuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("b", "g: " + consulta);
                   // Toast.makeText(this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();

                }



connection.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(parametros[1].equals("4")){
            try {
                url=new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();

                if (codigo_respuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("b", "g: " + consulta);

                }





            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(parametros[1].equals("5")){
            try {
                url=new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();
Log.e("ruta",""+url);
                if (codigo_respuesta == HttpURLConnection.HTTP_OK){
                    Log.e("p1","entro");
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("b", "g: " + consulta);

                }else{
                    Log.e("p2"," no entro");
                }





            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
////////////////        Prueaba de MOdificacion
        }else if(parametros[1].equals("6")){
            try {
                url = new URL(ruta);

                //se puede utilizar UrlConection y HttpURLConnection para la conexion
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();
                String documento = parametros[2];
                String nombre = parametros[3];
                String profesion = parametros[4];
                //buffer establecer la via de comunicacion
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                //ENvio de datos
                escritor.write("&documento="+documento +"&nombre=" + nombre + "&profesion="+profesion);
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    Log.e("daots", "guardo: " + consulta);
                }
                conexion.disconnect();
            } catch (Exception es) {
                Log.e("error", "guardo: " + consulta);
            }



        }
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
        return consulta;
    }
   // @Override protected void onPostExecute(String res) {
     //   progreso.dismiss();
       // Log.e("res" ,""+res);

    //}

}
