package com.example.tuqueres_santiago.controlador_deber;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class UsuarioSingletonVolley  {
    private RequestQueue queue;
    private Context context;
    private static UsuarioSingletonVolley miInstancia;

    public UsuarioSingletonVolley(Context context) {
        this.context = context;
        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if (queue == null)
            queue = Volley.newRequestQueue(context.getApplicationContext());

        return queue;
    }
    public static synchronized UsuarioSingletonVolley getInstance(Context context){
        if (miInstancia == null){
            miInstancia = new UsuarioSingletonVolley(context);
        }

        return miInstancia;
    }

    public <T>  void  addToRequestque(Request request){
        queue.add(request);

    }
}
