package com.example.tuqueres_santiago.modelo;

public interface Comunicador {
    public void responder(String datos);
}
