package com.example.tuqueres_santiago.modelo.config;

public class AlumnoSW {
    int id;
    String nombres;
    String dieccion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDieccion() {
        return dieccion;
    }

    public void setDieccion(String dieccion) {
        this.dieccion = dieccion;
    }
}
