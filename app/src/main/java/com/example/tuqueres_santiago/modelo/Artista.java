package com.example.tuqueres_santiago.modelo;

import java.util.Date;
import java.util.List;

public class Artista {

    List<Artista>lista;
    private String nombres;
    private String apellidos;
    private int foto;
    String nombre_artistico;
    private Date fechaNacimiento;

    public String getNombre_artistico() {
        return nombre_artistico;
    }

    public void setNombre_artistico(String nombre_artistico) {
        this.nombre_artistico = nombre_artistico;
    }

    public String getNombres() {
        return nombres;
    }

    public List<Artista> getLista() {
        return lista;
    }

    public void setLista(List<Artista> lista) {
        this.lista = lista;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
