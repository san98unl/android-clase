package com.example.tuqueres_santiago.modelo;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name="carro")//nombre que tendra la base de datos

public class Carro extends Model {
    @Column(name = "placa", unique = true)
    private  String placa;
    @Column(name = "modelo", notNull = true)
    private String modelo;
    @Column(name = "marca", notNull = true)
    private String marca;
    @Column(name = "año", notNull = true)
    int año;

    public Carro() {
        super();
    }

    public Carro(String placa, String modelo, String marca, int año) {
        super();
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.año = año;
    }

    public static List<Carro>getAllCar(){
        return new Select().from(Carro.class).execute();
    };
    public static void DeleteAllCar(){
        new Delete().from(Carro.class).execute();
    };

    public static Carro getPlateCar(String placa){
        return new Select().from(Carro.class).where("placa=?",placa).executeSingle();
    };
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
}
