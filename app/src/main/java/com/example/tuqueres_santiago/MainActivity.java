package com.example.tuqueres_santiago;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.vistas.actividades.ActicidadCarroORM;
import com.example.tuqueres_santiago.vistas.actividades.ActividadArchivos;
import com.example.tuqueres_santiago.vistas.actividades.ActividadEnviarParametro;
import com.example.tuqueres_santiago.vistas.actividades.ActividadEscucharFrag;
import com.example.tuqueres_santiago.vistas.actividades.ActividadLogin;
import com.example.tuqueres_santiago.vistas.actividades.ActividadMemoriaInterna;
import com.example.tuqueres_santiago.vistas.actividades.ActividadMemoriaPrograma;
import com.example.tuqueres_santiago.vistas.actividades.ActividadProductoHelper;
import com.example.tuqueres_santiago.vistas.actividades.ActividadRecyclerArtista;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSuma;
import com.example.tuqueres_santiago.vistas.fragmentos.Fragmento;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button botonLogin,BotonSumar,boton_parametros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //obtenemos el control de los componentes grficos
        cargarComponentes();
        ///////////////////////////7


    }

    private void cargarComponentes(){
        botonLogin = findViewById(R.id.btnIrlogin);
        BotonSumar = findViewById(R.id.btnIrSumar);
        boton_parametros = findViewById(R.id.btnIrParametros);
        botonLogin.setOnClickListener(this);
        BotonSumar.setOnClickListener(this);
        boton_parametros.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()){
                case R.id.btnIrlogin:
                    intent=new Intent(MainActivity.this, ActividadLogin.class);
                    startActivity(intent);
                    break;
                case R.id.btnIrSumar:
                    intent = new Intent(MainActivity.this, ActividadSuma.class);
                    startActivity(intent);
                    break;
                case R.id.btnIrParametros:
                    intent = new Intent(MainActivity.this, ActividadEnviarParametro.class);
                    startActivity(intent);
                    break;
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //metodo para cargar menus
        //Menu Inflarter permite crear un objeto para manejar el archivo xml
        //en nuestro caso es (main.xml)
        //el metodo inflate permite agrtegar un menu implementado de un archivo xml a la actividad
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.main, menu);
        inflaterMenu.inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //este metodo permite realizar eventos en cada item hijo de los menus
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent =new Intent(MainActivity.this,ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionSumar:
                intent =new Intent(MainActivity.this,ActividadSuma.class);
                startActivity(intent);
                break;
            case R.id.opcionParametros:
                intent =new Intent(MainActivity.this,ActividadEnviarParametro.class);
                startActivity(intent);
                break;
            case R.id.opcionColores:
                intent =new Intent(MainActivity.this, Fragmento.class);
                startActivity(intent);
                break;

            case R.id.opcionSum:
                final Dialog dlgSumar = new Dialog(MainActivity.this);
                dlgSumar.setContentView(R.layout.dlg_sumar);
                Button botonsumarDialog = dlgSumar.findViewById(R.id.btnSumar);
                final EditText cajaN1 = dlgSumar.findViewById(R.id.txtN1Dgl);
                final EditText cajaN2 = dlgSumar.findViewById(R.id.txtN2Dgl);
                botonsumarDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resultado = Integer.parseInt(cajaN1.getText().toString())+
                                Integer.parseInt(cajaN2.getText().toString());
                        Toast.makeText(MainActivity.this,"suma es"+resultado,Toast.LENGTH_SHORT).show();
                   dlgSumar.hide();
                    }
                });

                dlgSumar.show();
                break;
            case R.id.opcionArtista:
                intent = new Intent(MainActivity.this, ActividadRecyclerArtista.class);
                ////////////////////////////////////
                //View view = null;
                //LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                //view = inflater.inflate(R.layout.item_artista, null);

               View view = getLayoutInflater().inflate(R.layout.item_artista,null);
              //  Button prueba =  view.findViewById(R.id.btn_prueba);

             //   prueba.setOnClickListener(new View.OnClickListener() {
               //     @Override
                 //   public void onClick(View v) {

                   //     final Dialog dlgData = new Dialog(MainActivity.this);
                     //   dlgData.setContentView(R.layout.dlg_datos);
                      //  dlgData.show();
                    //}
                //});
                ///////////////////////////////////////
                startActivity(intent);
                break;
            //case R.id.opcionInterno:
                //intent = new Intent(MainActivity.this, ActividadMemoriaInterna.class);
                //startActivity(intent);
                //break;
            //case R.id.opcionPrograma:
                //intent = new Intent(MainActivity.this, ActividadMemoriaPrograma.class);
                //startActivity(intent);
                //break;
            case R.id.opcionArchivos:
                intent = new Intent(MainActivity.this, ActividadArchivos.class);
                startActivity(intent);
                break;
            case R.id.opcionEscuchar:
                intent = new Intent(MainActivity.this, ActividadEscucharFrag.class);
               startActivity(intent);
                break;
            case R.id.opcionSQLITE:
                intent = new Intent(MainActivity.this, ActividadProductoHelper.class);
                startActivity(intent);
                break;
            case R.id.opcionMenu:
                intent = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intent);
                break;
            case R.id.opcionORM:
                intent = new Intent(MainActivity.this, ActicidadCarroORM.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
