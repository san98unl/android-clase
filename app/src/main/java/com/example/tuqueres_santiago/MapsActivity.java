package com.example.tuqueres_santiago;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.example.tuqueres_santiago.controlador.LeerRaw;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.modelo.Coordenadas;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.activeandroid.Cache.getContext;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerClickListener,GoogleMap.OnInfoWindowClickListener
         {
             Button Satelite,Hibrido,Terreno,Posicion;
             List<Coordenadas>lista_coordenadas;
             List<Coordenadas>lista_coordenadas_final;
             LeerRaw leerRaw;


             private static final int LOCATION_REQUEST_CODE = 1;
    private GoogleMap mMap;
    ArrayList<LatLng> MarkerPoints;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        cargar_componente();



        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
             public void cargar_componente(){
                 Satelite=findViewById(R.id.btnSatelite);
                 Hibrido=findViewById(R.id.btnHibrido);
                 Terreno=findViewById(R.id.btnTerreno);
                 Posicion=findViewById(R.id.btnPosicion);
                 Satelite.setOnClickListener(this);
                 Hibrido.setOnClickListener(this);
                 Terreno.setOnClickListener(this);
                 Posicion.setOnClickListener(this);


             }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {
        leerRaw=new LeerRaw();
        lista_coordenadas_final=new ArrayList<Coordenadas>();

        mMap = googleMap;





        //mMap.getUiSettings().setZoomControlsEnabled(true);
        LatLng casa = new LatLng(-4.011886, -79.209377);
        LatLng uni = new LatLng(-4.032897, -79.202371);
        LatLng cali = new LatLng(3.4383, -76.5161);


        Marker marker1 = googleMap.addMarker(new MarkerOptions()
                .position(casa)
                .title("Inicio").snippet("inicia la ruta").icon(BitmapDescriptorFactory.fromResource(R.drawable.devices)));

mMap.setOnInfoWindowClickListener(this);

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(casa)
                .zoom(10)
                .build();

        googleMap.addMarker(new MarkerOptions()
                .position(uni).title("Fin").snippet("acaba la ruta").icon(BitmapDescriptorFactory.fromResource(R.drawable.student)));


        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//agregar_marcadores(googleMap,lista_coordenadas);
        String lectura=leerRaw.leer();
        //String currentString = "Fruit: they taste good.very nice actually";

        //String[] separated = currentString.split(":");


        String temp[]=lectura.split(";");




        PolylineOptions opcionesPoliLinea = new PolylineOptions();
        Log.e("paso1","paso1");
        lista_coordenadas=new ArrayList<Coordenadas>();

        String[] listaA1;
        //String[] listaA = cad.split(";");
        Log.e("paso2","paso2");
        for (int i=0; i<temp.length; i++){
            Coordenadas a = new Coordenadas();
            listaA1= temp[i].split(",");
            a.setLatitud(Double.parseDouble(listaA1[0]));
            Log.e("lat",listaA1[0]);

            a.setLongitud(Double.parseDouble(listaA1[1]));
            Log.e("long",listaA1[1]);
            lista_coordenadas.add(a);
        }
        Log.e("paso3","paso3");
       agregar_marcadores(googleMap,lista_coordenadas);

        Toast.makeText(getContext(), temp.length+"", Toast.LENGTH_SHORT).show();
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(0).getLatitud(), lista_coordenadas.get(0).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(1).getLatitud(), lista_coordenadas.get(1).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(2).getLatitud(), lista_coordenadas.get(2).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(3).getLatitud(), lista_coordenadas.get(3).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(4).getLatitud(), lista_coordenadas.get(4).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(5).getLatitud(), lista_coordenadas.get(5).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(6).getLatitud(), lista_coordenadas.get(6).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(7).getLatitud(), lista_coordenadas.get(7).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(8).getLatitud(), lista_coordenadas.get(8).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(9).getLatitud(), lista_coordenadas.get(9).getLongitud()));
        opcionesPoliLinea.add(new LatLng(lista_coordenadas.get(10).getLatitud(), lista_coordenadas.get(10).getLongitud()));

        Polyline polyline = mMap.addPolyline(opcionesPoliLinea);
        polyline.setColor(Color.RED);



        CircleOptions circleOptions = new CircleOptions()
                .center(new LatLng(lista_coordenadas.get(0).getLatitud(), lista_coordenadas.get(0).getLongitud()));
        circleOptions.radius(300.048); // In meters

        mMap.addCircle(circleOptions);


        CircleOptions circleOptionsfin = new CircleOptions()
                .center(new LatLng(lista_coordenadas.get(10).getLatitud(), lista_coordenadas.get(10).getLongitud()));
        circleOptionsfin.radius(300.048); // In meters

        mMap.addCircle(circleOptionsfin);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(LayoutInflater.from(this)));

    }

    public void agregar_marcadores(GoogleMap googleMap,List<Coordenadas>lista){
        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(1).getLatitud(),lista_coordenadas.get(1).getLongitud())).title("Tienda Portugal").snippet("segundi"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(2).getLatitud(),lista_coordenadas.get(2).getLongitud())).title("Cafeteria Flor de Azucar").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(3).getLatitud(),lista_coordenadas.get(3).getLongitud())).title("Cooperativa Policia Nacional").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(4).getLatitud(),lista_coordenadas.get(4).getLongitud())).title("Chifa Ban Kok Loja").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(5).getLatitud(),lista_coordenadas.get(5).getLongitud())).title("Lojanita Mia").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(6).getLatitud(),lista_coordenadas.get(6).getLongitud())).title("La Canela").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(7).getLatitud(),lista_coordenadas.get(7).getLongitud())).title("Colegio 27 de Febrero").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(8).getLatitud(),lista_coordenadas.get(8).getLongitud())).title("Plus Tv").snippet("tercero"));

        googleMap.addMarker(new MarkerOptions()
                .position( new LatLng(lista_coordenadas.get(9).getLatitud(),lista_coordenadas.get(9).getLongitud())).title("Clinica San Pablo").snippet("tercero"));

    }
             private void obtenerPosicion()
             {
                 CameraPosition camPos = mMap.getCameraPosition();

                 LatLng coordenadas = camPos.target;
                 double latitud = coordenadas.latitude;
                 double longitud = coordenadas.longitude;

                 Toast.makeText(this, "Lat: " + latitud + " | Long: " + longitud, Toast.LENGTH_SHORT).show();
             }



             @Override
             public void onClick(View v) {
                 switch (v.getId()) {
                     case R.id.btnHibrido:
                         mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                         mMap.getUiSettings().setZoomControlsEnabled(true);
                         mMap.setMyLocationEnabled(true);


                         Log.e("e","ddd");

                         break;
                     case R.id.btnSatelite:
                         mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                         mMap.getUiSettings().setZoomControlsEnabled(true);
                         break;
                     case R.id.btnTerreno:
                         mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                         mMap.getUiSettings().setZoomControlsEnabled(true);
                         break;
                     case R.id.btnPosicion:
                         obtenerPosicion();

                         break;
                 }
             }

             @Override
             public boolean onMarkerClick(Marker marker) {

                 return false;
             }

             @Override
             public void onInfoWindowClick(Marker marker) {
                 LayoutInflater imagen_Alert =LayoutInflater.from(this);

                 final View vista= imagen_Alert.inflate(R.layout.imagen,null);
                 final View vista3= imagen_Alert.inflate(R.layout.imagen3,null);
                 final View vista2= imagen_Alert.inflate(R.layout.imagen2,null);
                 final View vista4= imagen_Alert.inflate(R.layout.imagen4,null);
                 final View vista5= imagen_Alert.inflate(R.layout.imagen5,null);
                 final View vista6= imagen_Alert.inflate(R.layout.imagen6,null);
                 final View vista7= imagen_Alert.inflate(R.layout.imagen7,null);
                 final View vista8= imagen_Alert.inflate(R.layout.imagen8,null);








                if (marker.getTitle().equals("Inicio")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi casa");
                    dialogo1.setView(vista);
                    dialogo1.setMessage("Lugar conde vivo");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                           // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("Cafeteria Flor de Azucar")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista3);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("Tienda Portugal")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista2);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("Cooperativa Policia Nacional")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista4);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                } else if (marker.getTitle().equals("Chifa Ban Kok Loja")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista5);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("Lojanita Mia")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista6);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("La Canela")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista7);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }else if (marker.getTitle().equals("Colegio 27 de Febrero")){
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
                    dialogo1.setTitle("Mi cafe");
                    dialogo1.setView(vista8);
                    dialogo1.setMessage("Tienda cerca de mi casa");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //aceptar();
                        }
                    });
                    dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            // cancelar();
                        }
                    });
                    dialogo1.show();
                }
                else{
                    Toast.makeText(this,"no esta",Toast.LENGTH_SHORT).show();
                }
             }
         }
