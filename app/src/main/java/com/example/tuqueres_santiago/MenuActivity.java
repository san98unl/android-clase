package com.example.tuqueres_santiago;

import android.content.Intent;
import android.os.Bundle;

import com.example.tuqueres_santiago.vistas.actividades.ActicidadCarroORM;
import com.example.tuqueres_santiago.vistas.actividades.ActividadLogin;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSWAlumnos;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSWClima;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSWVolleyAlumno;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSensorLuz;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSensorProximidad;
import com.example.tuqueres_santiago.vistas.actividades.ActividadSensores;
import com.example.tuqueres_santiago.vistas.actividades.DeberHilos;
import com.example.tuqueres_santiago.vistas.actividades.DeberVolley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

public class MenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //este metodo permite realizar eventos en cada item hijo de los menus
        Intent intent;
        switch (item.getItemId()){
            case R.id.actividadLogin:
                intent =new Intent(MenuActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        Intent intent;
        if (id == R.id.nav_ORM) {

            intent = new Intent(MenuActivity.this, ActicidadCarroORM.class);
            startActivity(intent);
        } else if (id == R.id.nav_menu) {
            intent = new Intent(MenuActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {
        } else if (id == R.id.nav_sw) {
            intent = new Intent(MenuActivity.this, ActividadSWAlumnos.class);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {

        }else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }else if (id == R.id.nav_sw_clima) {
            intent = new Intent(MenuActivity.this, ActividadSWClima.class);
            startActivity(intent);
        }else if (id == R.id.nav_sw_volley_alumno) {
            intent = new Intent(MenuActivity.this, ActividadSWVolleyAlumno.class);
            startActivity(intent);
        }else if (id == R.id.nav_deber_hilos) {
            intent = new Intent(MenuActivity.this, DeberHilos.class);
            startActivity(intent);
        }else if (id == R.id.nav_deber_volley) {
            intent = new Intent(MenuActivity.this, DeberVolley.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_maps) {
            intent = new Intent(MenuActivity.this, MapsActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_sensores) {
            intent = new Intent(MenuActivity.this, ActividadSensores.class);
            startActivity(intent);
        }else if (id == R.id.nav_sensor_proximidad) {
            intent = new Intent(MenuActivity.this, ActividadSensorProximidad.class);
            startActivity(intent);
        }else if (id == R.id.nav_sensor_luz) {
            intent = new Intent(MenuActivity.this, ActividadSensorLuz.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
