package com.example.tuqueres_santiago;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = "CustomInfoWindowAdapter";
    private LayoutInflater inflater;
    public CustomInfoWindowAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        //Carga layout personalizado.

        View v = inflater.inflate(R.layout.info_window, null);
        String[] info = marker.getTitle().split("&");
        String url = marker.getSnippet();
        ImageView imageView= v.findViewById(R.id.info_window_imagen);

        if (marker.getTitle().equals("Inicio")){
            imageView.setImageResource(R.drawable.d1);

            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Casa");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi casa");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Ahi vivo jejeje");
        }else if(marker.getTitle().equals("Fin")){
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Universidad Nacional de Loja");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi uniiversidad");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Tienda Portugal")){
            imageView.setImageResource(R.drawable.tienda_portugal);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Tienda Portugal");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Tienda");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Cafeteria Flor de Azucar")){
            imageView.setImageResource(R.drawable.cafeteria_azucar);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Cafeteria Flor de Azucar");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Tienda");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Cooperativa Policia Nacional")){
            imageView.setImageResource(R.drawable.cooperativa_policia);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Cooperativa Policia Nacional");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Coop");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Chifa Ban Kok Loja")){
            imageView.setImageResource(R.drawable.chifa_bankok);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Chifa Ban Kok Loja");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Chifa");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Lojanita Mia")){
            imageView.setImageResource(R.drawable.lojanita);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Lojanita Mia");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi restaurante");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("La Canela")){
            imageView.setImageResource(R.drawable.canela);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("La Canela");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Tienda");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Colegio 27 de Febrero")){
            imageView.setImageResource(R.drawable.cole_27_febrero);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Colegio 27 de Febrero");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Colegio");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Plus Tv")){
            imageView.setImageResource(R.drawable.plustv);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Plus Tv");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi Tienda");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }else if(marker.getTitle().equals("Clinica San Pablo")){
            imageView.setImageResource(R.drawable.san_pablo);
            ((TextView)v.findViewById(R.id.info_window_nombre)).setText("Clinica San Pablo");
            ((TextView)v.findViewById(R.id.info_window_placas)).setText("Descripcion: Mi clinica");
            ((TextView)v.findViewById(R.id.info_window_estado)).setText("Detalle: Activo");
        }


        return v;
    }
}
