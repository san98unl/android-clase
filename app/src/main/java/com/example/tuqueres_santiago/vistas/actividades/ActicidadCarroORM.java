package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Carro;
import com.example.tuqueres_santiago.modelo.Producto;
import com.example.tuqueres_santiago.vistas.adapter.CarroAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActicidadCarroORM extends AppCompatActivity implements View.OnClickListener {
EditText caja_placa;
EditText caja_modelo;
EditText caja_marca;
EditText caja_año;
ImageButton guardar,buscarr;
Button boton_eliminar,boton_eliminar_placa,boton_buscar_placa,boton_modificar;

    CarroAdapter adapter;
    List<Carro> lista;
    List<Carro> lista_temp;
    List<Carro> lista_parcial;
    RecyclerView recicler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acticidad_carro_orm);
        cargar_componente();
    }
    public void cargar_componente(){
        caja_placa=findViewById(R.id.txtPlacaOrm);
        caja_modelo=findViewById(R.id.txtModeloOrm);
        caja_marca=findViewById(R.id.txtMarcaOrm);
        caja_año=findViewById(R.id.txtAñoOrm);
        guardar=findViewById(R.id.imagebtnSave);
        buscarr=findViewById(R.id.imagebtnBuscarTodosORM);
        boton_buscar_placa=findViewById(R.id.btnBuscarPlacaORM);
        boton_eliminar=findViewById(R.id.btnEliminarTodos);
        boton_eliminar_placa=findViewById(R.id.btnEliminarORMPlaca);
        boton_modificar=findViewById(R.id.btnModificarORM);
        recicler=findViewById(R.id.recicler_carro);

boton_buscar_placa.setOnClickListener(this);
boton_eliminar.setOnClickListener(this);
boton_eliminar_placa.setOnClickListener(this);
boton_modificar.setOnClickListener(this);
        guardar.setOnClickListener(this);
        buscarr.setOnClickListener(this);

    }
    public void cargar_datos(View v){

        String cod= lista.get(recicler.getChildAdapterPosition(v)).getPlaca();
        Log.e("cod",cod+"");
        Carro ca = new Carro();
        ca=Carro.getPlateCar(cod);

            caja_año.setText(""+ca.getAño());
            caja_marca.setText(ca.getMarca());
            caja_modelo.setText(ca.getModelo());
            caja_placa.setText(ca.getPlaca());


    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imagebtnSave:
                Carro carro = new Carro();
                carro.setPlaca(caja_placa.getText().toString());
                carro.setMarca(caja_marca.getText().toString());
                carro.setAño(Integer.parseInt(caja_año.getText().toString()));
                carro.setModelo(caja_modelo.getText().toString());
                carro.save();
                Toast.makeText(this, "guardadoORM ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imagebtnBuscarTodosORM:
                lista= new ArrayList<Carro>();
                lista=Carro.getAllCar();
                adapter = new CarroAdapter(lista);
                // Log.e("e",hp.getAll().get(0).getCantidad()+"");
                recicler.setLayoutManager(new LinearLayoutManager(this));

                adapter.setOnclickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cargar_datos(v);


                    }
                });
                recicler.setAdapter(adapter);
                Toast.makeText(this, "carros " + Carro.getAllCar().size(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnBuscarPlacaORM:
                lista_temp= new ArrayList<Carro>();
                Carro car1 = new Carro();
                car1=Carro.getPlateCar(caja_placa.getText().toString());
                lista_temp.add(car1);
                adapter = new CarroAdapter(lista_temp);
                // Log.e("e",hp.getAll().get(0).getCantidad()+"");
                recicler.setLayoutManager(new LinearLayoutManager(this));
                recicler.setAdapter(adapter);
                Toast.makeText(this, "Placa " + Carro.getPlateCar(caja_placa.getText().toString()).getPlaca() +
                        "marca " + Carro.getPlateCar(caja_placa.getText().toString()).getMarca() + " modelo " + Carro.getPlateCar(caja_placa.getText().toString()).getModelo() +
                        " Año " + Carro.getPlateCar(caja_placa.getText().toString()).getAño(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnEliminarTodos:

                Carro.DeleteAllCar();

                Toast.makeText(this, "eliminadoCarrosORM ", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btnEliminarORMPlaca:
                Carro car= new Carro();
                car=Carro.getPlateCar(caja_placa.getText().toString());
                car.delete();
                Toast.makeText(this, "eliminadoORM ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnModificarORM:
                Carro cars= new Carro();
                cars=Carro.getPlateCar(caja_placa.getText().toString());
                cars.setMarca(caja_marca.getText().toString());
                cars.setAño(Integer.parseInt(caja_año.getText().toString()));
                cars.setModelo(caja_modelo.getText().toString());
                cars.save();
                Toast.makeText(this, "MosidicadoORM ", Toast.LENGTH_SHORT).show();
                break;


        }
        }
    }

