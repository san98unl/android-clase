package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;

public class ActividadLogin extends AppCompatActivity implements View.OnClickListener {
    EditText caja_usuario,caja_password;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_login);
        cargarComponentes();
    }
    private void cargarComponentes(){
        caja_usuario = findViewById(R.id.txt_usuario);
        caja_password = findViewById(R.id.txt_password);
        boton=findViewById(R.id.btnlogin);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText( ActividadLogin.this," Usuario "+caja_usuario.getText()+" Clave "+caja_password.getText(),
                Toast.LENGTH_SHORT).show();
    }
}
