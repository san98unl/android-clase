package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.controlador_deber.ControladorHilosDeber;
import com.example.tuqueres_santiago.modelo.Alumno;
import com.example.tuqueres_santiago.modelo.Usuario;
import com.example.tuqueres_santiago.vistas.adapter.AlumnoAdapter;
import com.example.tuqueres_santiago.vistas.adapter.UsuarioAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DeberHilos extends AppCompatActivity implements View.OnClickListener {
    Button boton_listar,boton_guardar,boton_update,boton_buscar_documento,boton_delete;
    EditText caja_documento,caja_nombre,caja_profesion;

    List<Usuario> lista;
    List<Usuario> lista_parcial;

    ControladorHilosDeber sw;
    RecyclerView recicler;
    UsuarioAdapter adapter;


    ////URLS
    String host= "http://192.168.1.36/tuqueres_santiago/";
    String get_lista = "wsJSONConsultarLista.php";
    String registro="wsJSONRegistro.php?documento=";
    String buscar="wsJSONConsultarUsuario.php?documento=";
    String delete="wsJSONDeleteMovil.php?documento=";
    String update="wsJSONUpdateMovil.php?documento=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deber_hilos);
        cargar_componentes();
    }
    public void cargar_componentes(){
        caja_documento=findViewById(R.id.txt_hilos_documento);
        caja_nombre=findViewById(R.id.txt_hilos_nombre);
        caja_profesion=findViewById(R.id.txt_hilos_profesion);
        boton_listar=findViewById(R.id.btn_listar_hilos);
        recicler=findViewById(R.id.rec_hilos);
        boton_guardar=findViewById(R.id.btn_guardar_hilos);
        boton_update=findViewById(R.id.btn_update_hilos);
        boton_buscar_documento=findViewById(R.id.btn_buscar_documento);
        boton_delete=findViewById(R.id.btn_delete_hilos);
        boton_guardar.setOnClickListener(this);
        boton_update.setOnClickListener(this);
        boton_listar.setOnClickListener(this);
        boton_buscar_documento.setOnClickListener(this);
        boton_delete.setOnClickListener(this);
        lista=new ArrayList<Usuario>();

    }
    private void cargar_datos(View v){

ControladorHilosDeber sw2 = new ControladorHilosDeber(this);
        try {
            int cod= lista.get(recicler.getChildAdapterPosition(v)).getDocumento();
            //Usuario ca = new Usuario();
            String d= sw2.execute(host.concat(buscar)+cod,"3").get();
            JSONObject jsonjObject = new JSONObject(d);
            //String valorLlave = jsonjObject.getString("usuario");
            JSONArray jsonArray = jsonjObject.getJSONArray("usuario");
            // JSONObject jObject2 = jsonjObject.getJSONObject("coord");
            Log.e("arra",jsonArray+"");
            caja_documento.setText(jsonArray.getJSONObject(0).getString("documento"));
            caja_nombre.setText(jsonArray.getJSONObject(0).getString("nombre"));
            caja_profesion.setText(jsonArray.getJSONObject(0).getString("profesion"));

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    };

    public List<Usuario>listar (String consulta) throws JSONException {
        JSONObject jsonjObject = null;
        try {
            jsonjObject = new JSONObject(consulta);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //String valorLlave = jsonjObject.getString("usuario");
        JSONArray jsonArray = jsonjObject.getJSONArray("usuario");
        // JSONObject jObject2 = jsonjObject.getJSONObject("coord");

        // Log.e("mensaje1", jObject2.toString());
        Log.e("mensaje", jsonArray+"");
        for (int i = 0; i < jsonArray.length(); i++)
        {
            try {
                Usuario alumno=new Usuario();
                JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                alumno.setDocumento(Integer.parseInt(jsonObjectHijo.get("documento").toString()));
                alumno.setNombre(jsonObjectHijo.get("nombre").toString());
                alumno.setProfesion(jsonObjectHijo.get("direccion").toString());

                lista.add(alumno);

                //Log.e("u",jsonObjectHijo.get("idalumno")+"");
            } catch (JSONException e) {
                Log.e("Parser JSON", e.toString());
            }
        }
        return lista;
    }

    @Override
    public void onClick(View v) {
        sw = new ControladorHilosDeber(this);
        switch (v.getId()) {
            case R.id.btn_listar_hilos:
                lista=new ArrayList<Usuario>();
                try {

                    String s=sw.execute(host.concat(get_lista),"1").get();
                    JSONObject jsonjObject = new JSONObject(s);
                    //String valorLlave = jsonjObject.getString("usuario");
                    JSONArray jsonArray = jsonjObject.getJSONArray("usuario");
                    // JSONObject jObject2 = jsonjObject.getJSONObject("coord");

                    // Log.e("mensaje1", jObject2.toString());
                    Log.e("mensaje", jsonArray+"");
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        try {
                            Usuario alumno=new Usuario();
                            JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                            alumno.setDocumento(Integer.parseInt(jsonObjectHijo.get("documento").toString()));
                            alumno.setNombre(jsonObjectHijo.get("nombre").toString());
                            alumno.setProfesion(jsonObjectHijo.get("profesion").toString());

                             lista.add(alumno);

                            //Log.e("u",jsonObjectHijo.get("idalumno")+"");
                        } catch (JSONException e) {
                            Log.e("Parser JSON", e.toString());
                        }
                    }

                }catch (Exception ex){
                    Log.e("error",ex+"");
                }
                adapter = new UsuarioAdapter(lista);
                recicler.setLayoutManager(new LinearLayoutManager(this));
                adapter.setOnclickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cargar_datos(v);



                    }
                });
                recicler.setAdapter(adapter);

                //Log.e("e",lista.get(0).getNombre()+"");

                break;
            case R.id.btn_guardar_hilos:
                try {
                    String  doc= caja_documento.getText().toString();
                    String pro= caja_profesion.getText().toString();
                    String nom=caja_nombre.getText().toString();

                    sw.execute(host.concat(registro)+caja_documento.getText().toString()+"&nombre="+caja_nombre.getText().toString()+"&profesion="+caja_profesion.getText().toString(),"2");
                   // sw.execute(host.concat(registro),"6",doc,pro,nom);

                    Toast.makeText(this,"guardado",Toast.LENGTH_SHORT).show();
                    // JSONObject jObject2 = jsonjObject.getJSONObject("coord");
                   // Log.e("error",host.concat(registro)+caja_documento+"&nombre="+caja_nombre+"&profesion="+caja_profesion+"");



                }catch (Exception ex){
                    Log.e("error",ex+"");
                }

                break;
            case R.id.btn_update_hilos:
                try {

                    sw.execute(host.concat(update),"6",caja_documento.getText().toString(),caja_nombre.getText().toString(),caja_profesion.getText().toString());
                   // Toast.makeText(this,"guarda",Toast.LENGTH_SHORT).show();
                    // JSONObject jObject2 = jsonjObject.getJSONObject("coord");
                    // Log.e("error",host.concat(registro)+caja_documento+"&nombre="+caja_nombre+"&profesion="+caja_profesion+"");
                    Toast.makeText(DeberHilos.this, "Actuializado Exitosomente", Toast.LENGTH_SHORT).show();



                }catch (Exception ex){
                    Log.e("error",ex+"");
                }

                break;
            case R.id.btn_buscar_documento:
                lista_parcial=new ArrayList<Usuario>();
                try {
                    String s=sw.execute(host.concat(buscar)+caja_documento.getText().toString(),"3").get();
                    JSONObject jsonjObject = new JSONObject(s);
                    //String valorLlave = jsonjObject.getString("usuario");
                    JSONArray jsonArray = jsonjObject.getJSONArray("usuario");
                    // JSONObject jObject2 = jsonjObject.getJSONObject("coord");

                    // Log.e("mensaje1", jObject2.toString());
                    Log.e("mensaje", jsonArray+"");
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        try {
                            Usuario alumno=new Usuario();
                            JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                            alumno.setDocumento(Integer.parseInt(jsonObjectHijo.get("documento").toString()));
                            alumno.setNombre(jsonObjectHijo.get("nombre").toString());
                            alumno.setProfesion(jsonObjectHijo.get("profesion").toString());

                            lista_parcial.add(alumno);

                            //Log.e("u",jsonObjectHijo.get("idalumno")+"");
                        } catch (JSONException e) {
                            Log.e("Parser JSON", e.toString());
                        }
                        adapter = new UsuarioAdapter(lista_parcial);
                        recicler.setLayoutManager(new LinearLayoutManager(this));

                        recicler.setAdapter(adapter);
                    }

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_delete_hilos:
                try {

                    sw.execute(host.concat(delete)+caja_documento.getText().toString(),"4");
                    Toast.makeText(this,"eliminado",Toast.LENGTH_SHORT).show();
                    // JSONObject jObject2 = jsonjObject.getJSONObject("coord");
                    // Log.e("error",host.concat(registro)+caja_documento+"&nombre="+caja_nombre+"&profesion="+caja_profesion+"");



                }catch (Exception ex){
                    Log.e("error",ex+"");
                }

                break;


        }
    }
}
