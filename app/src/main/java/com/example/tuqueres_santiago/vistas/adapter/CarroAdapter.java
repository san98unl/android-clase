package com.example.tuqueres_santiago.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Carro;
import com.example.tuqueres_santiago.modelo.Godo;

import java.util.List;

public class CarroAdapter  extends RecyclerView.Adapter<CarroAdapter.ViewHolderCarro> implements View.OnClickListener {
    final List<Carro> lista;

    private View.OnClickListener clickCarro;

    public CarroAdapter(List<Carro> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolderCarro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carro,null);
        view.setOnClickListener(this);
        return new ViewHolderCarro(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCarro holder, int position) {
        holder.datoPlaca.setText(lista.get(position).getPlaca()+"");
        holder.datoMarca.setText(lista.get(position).getMarca()+"");
        holder.datoAño.setText(lista.get(position).getAño()+"");
        holder.datoModelo.setText(lista.get(position).getModelo()+"");
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    @Override
    public void onClick(View v) {
        if (clickCarro!=null){
            clickCarro.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener listener){
        this.clickCarro=listener;
    }
    public class ViewHolderCarro extends RecyclerView.ViewHolder {
        TextView datoModelo;
        TextView datoMarca;
        TextView datoPlaca;
        TextView datoAño;
        public ViewHolderCarro(@NonNull View itemView) {
            super(itemView);
            datoModelo=itemView.findViewById(R.id.lblModeloORM);
            datoAño=itemView.findViewById(R.id.lblAñoORM);
            datoMarca=itemView.findViewById(R.id.lblMarcaORM);
            datoPlaca=itemView.findViewById(R.id.lblPlacaORM);
        }
    }
}
