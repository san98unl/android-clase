package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;

public class ActividadRecibirParametros extends AppCompatActivity {
    TextView recibir_nombre,recibir_apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recibir_parametros);

        recibir_nombre =  findViewById(R.id.nombreRecibir);
        recibir_apellido =  findViewById(R.id.apellidoRecibir);


        Bundle parametros = this.getIntent().getExtras();
        if(parametros !=null){
            String datos = parametros.getString("nombres");
            String datos2 = parametros.getString("apellidos");
            recibir_nombre.setText(datos);
            recibir_apellido.setText(datos2);
        }
    }

}
