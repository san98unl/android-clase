package com.example.tuqueres_santiago.vistas.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoPrograma.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoPrograma#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoPrograma extends Fragment implements View.OnClickListener {
    Button boton_mostrar;
    //TextView mostrar;
    RecyclerView recicler_mostrar_P;


    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> lista_final;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoPrograma() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoPrograma.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoPrograma newInstance(String param1, String param2) {
        FragmentoPrograma fragment = new FragmentoPrograma();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    public void cargar_recicler(String cad){
        listarArtistas = new ArrayList<Artista>();
        String[] listaA1;
        String[] listaA = cad.split(";");

        for (int i=0; i<listaA.length; i++){
            Artista a = new Artista();
            listaA1= listaA[i].split(",");
            a.setNombres(listaA1[0]);
            a.setApellidos(listaA1[1]);
            listarArtistas.add(a);
        }
        adapter = new ArtistaAdapter(listarArtistas);
        recicler_mostrar_P.setLayoutManager(new LinearLayoutManager(getContext()));
        recicler_mostrar_P.setAdapter(adapter);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.fragment_fragmento_programa, container, false);
        recicler_mostrar_P = vista.findViewById(R.id.recycler_lista);
        boton_mostrar=vista.findViewById(R.id.btnMP_mostrar);
        //mostrar = vista.findViewById(R.id.lbl_mostrar);
        boton_mostrar.setOnClickListener(this);
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnMP_mostrar:

                try{
                    InputStream input = getResources().openRawResource(R.raw.archivo_raw);

                    BufferedReader lector = new BufferedReader(new InputStreamReader(input));

                    String cadena = lector.readLine();
                    //mostrar.setText(cadena);
                    cargar_recicler(cadena);
                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error de escritura"+ex.getMessage());
                }
                break;


        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
