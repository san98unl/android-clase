package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;

public class ActividadSuma extends AppCompatActivity implements View.OnClickListener {
    EditText n1,n2,resultado;
    Button boton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_suma);
        cargarComponentes();
    }
    private void cargarComponentes(){
        n1=findViewById(R.id.txtn1);
        n2=findViewById(R.id.txtn2);
        boton=findViewById(R.id.btncalcular);
        resultado=findViewById(R.id.txtsuma);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
       int valor1 = Integer.parseInt(n1.getText().toString());
        int valor2 = Integer.parseInt(n2.getText().toString());
        int suma =valor1+valor2;
        resultado.setText(suma+"");
    }
}
