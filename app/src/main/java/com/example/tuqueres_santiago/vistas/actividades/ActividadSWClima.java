package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.controlador.ControladorServicio;
import com.example.tuqueres_santiago.vistas.adapter.AlumnoAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ActividadSWClima extends AppCompatActivity implements View.OnClickListener {
Button btn_go;
TextView respuesta;
    ControladorServicio sw;
    String host= "https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swclima);
        cargar();
    }

    @Override
    public void onClick(View v) {
        sw = new ControladorServicio();
        switch (v.getId()) {
            case R.id.btnGo:
              //  sw.execute(host,"1");//ejecuta el hilo en el Do inBackground
               try {
                    String s=sw.execute(host,"1").get();

                    JSONObject j1= new JSONObject(s);
                    String c = j1.getString("coord");
                   String d = j1.getString("weather");
                   String e = j1.getString("main");
                   String g = j1.getString("visibility");
                   String f = j1.getString("sys");
                   String h = j1.getString("wind");
                   String i = j1.getString("clouds");
                   //String j = j1.getString("country");


                    respuesta.setText("coordenadas"+c+"\n"+"weather"+d+"\n"+"main"+e+"\n"+"sys"+f+"\n"+"visibilidad"+g+"\n"+"wind"+h+"\n"+"clouds"+i+"\n");
                    Log.e("s ",c);
                } catch (ExecutionException e) {
                    e.printStackTrace();
               } catch (InterruptedException e) {
                    e.printStackTrace();
               } catch (JSONException e) {
                    e.printStackTrace();
               }

                break;


        }
    }
    public void cargar(){
        btn_go=findViewById(R.id.btnGo);
        btn_go.setOnClickListener(this);
        respuesta=findViewById(R.id.lbl_response);
    }
}
