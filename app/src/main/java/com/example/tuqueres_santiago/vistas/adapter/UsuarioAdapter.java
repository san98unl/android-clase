package com.example.tuqueres_santiago.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Producto;
import com.example.tuqueres_santiago.modelo.Usuario;

import java.util.List;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.ViewHolderUsuario> implements View.OnClickListener {
    final List<Usuario> lista;
    private View.OnClickListener clickUsuario;

    public UsuarioAdapter(List<Usuario> lista) {
        this.lista = lista;
    }

    @Override
    public void onClick(View v) {
        if (clickUsuario!=null){
            clickUsuario.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener listener){
        this.clickUsuario=listener;
    }
    @NonNull
    @Override
    public UsuarioAdapter.ViewHolderUsuario onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario,null);
        view.setOnClickListener(this);
        return new ViewHolderUsuario(view);

    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioAdapter.ViewHolderUsuario holder, int position) {
        holder.datoDocumento.setText(lista.get(position).getDocumento()+"");
        holder.datoNombre.setText(lista.get(position).getNombre()+"");
        holder.datoProfesion.setText(lista.get(position).getProfesion()+"");
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderUsuario extends RecyclerView.ViewHolder {
        TextView datoDocumento;
        TextView datoNombre;
        TextView datoProfesion;
        //TextView datoCantidad;
        public ViewHolderUsuario(@NonNull View itemView) {
            super(itemView);
            datoDocumento=itemView.findViewById(R.id.lbl_documento);
            datoNombre=itemView.findViewById(R.id.lbl_nombre);
            datoProfesion=itemView.findViewById(R.id.lbl_profesion);
        }
    }
}
