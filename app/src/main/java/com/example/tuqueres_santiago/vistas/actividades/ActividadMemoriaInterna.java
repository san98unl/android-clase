package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActividadMemoriaInterna extends AppCompatActivity implements View.OnClickListener {
Button botonGuardar;
Button buscar_todos;
Button botonMostrar;
EditText caja_nombres;
EditText caja_apellidos;
EditText caja_nombre_artistico;
TextView datos;
    RecyclerView recycler;
    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> lista_final;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);
        tomar_control();
    }
    public void tomar_control(){
        listarArtistas = new ArrayList<Artista>();
        lista_final = new ArrayList<Artista>();
        botonMostrar = findViewById(R.id.btnMostrar);
        botonGuardar = findViewById(R.id.btnGuardarMI);
        buscar_todos= findViewById(R.id.btnBuscarTodos);
        caja_nombres=findViewById(R.id.txtNombresMI);
        caja_apellidos = findViewById(R.id.txtApellidosMI);
        caja_nombre_artistico = findViewById(R.id.txtNombreArtistico);
        recycler=findViewById(R.id.resultado);
        datos = findViewById(R.id.lbldata);
        botonGuardar.setOnClickListener(this);
        buscar_todos.setOnClickListener(this);
        botonMostrar.setOnClickListener(this);
    }
private String[] cargar_lista(String lista){
        String[] partes =lista.split(";");
return partes;
}
    @Override
    public void onClick(View v) {




        switch (v.getId()){
            case R.id.btnGuardarMI:
                try{
                    Artista artista1 = new Artista();
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));


                    artista1.setNombres(caja_nombres.getText().toString());
                    artista1.setApellidos(caja_apellidos.getText().toString());
                    artista1.setNombre_artistico(caja_nombre_artistico.getText().toString());
                    artista1.setFoto(R.drawable.descarga);
                    listarArtistas.add(artista1);
                    escritor.write(listarArtistas.get(0).getNombres()+","+listarArtistas.get(0).getApellidos()+","+listarArtistas.get(0).getNombre_artistico()+";");
                    datos.setText("guardado");
                   // adapter = new ArtistaAdapter(listarArtistas);
                    //recy.setLayoutManager(new LinearLayoutManager(this));
                    //recycler.setAdapter(adapter);
                escritor.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error de escritura"+ex.getMessage());
                }
                break;
                case  R.id.btnBuscarTodos:
                    try{
                        BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                        String lineas = lector.readLine();
                        datos.setText(lineas);
                        Log.e("prueba",lineas);
                        lector.close();
                    }catch (Exception ex){
                        Log.e("Archivo MI","Error de lectura"+ex.getMessage());
                    }
                    break;
            case  R.id.btnMostrar:






               adapter = new ArtistaAdapter(listarArtistas);
                recycler.setLayoutManager(new LinearLayoutManager(this));
                recycler.setAdapter(adapter);
              break;
        }

    }
}
