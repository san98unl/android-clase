package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Comunicador;
import com.example.tuqueres_santiago.vistas.fragmentos.Parcial1;
import com.example.tuqueres_santiago.vistas.fragmentos.Parcial2;

public class ActividadEscucharFrag extends AppCompatActivity implements Comunicador, View.OnClickListener,Parcial1.OnFragmentInteractionListener, Parcial2.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_escuchar_frag);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void responder(String datos) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Parcial2 frgr1 = (Parcial2) fragmentManager.findFragmentById(R.id.fragment3);
        frgr1.cambiarTexto(datos);
    }
}
