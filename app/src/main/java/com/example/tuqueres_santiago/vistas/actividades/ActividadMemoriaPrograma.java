package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class ActividadMemoriaPrograma extends AppCompatActivity implements View.OnClickListener {
Button boton_mostrar;
TextView mostrar;
RecyclerView recicler_mostrar_P;


    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> lista_final;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_programa);
        tomar_control();
    }
    public void tomar_control(){
        recicler_mostrar_P = findViewById(R.id.recycler_mostrar);
        boton_mostrar=findViewById(R.id.btnMP_mostrar);
        mostrar = findViewById(R.id.lbl_mostrar);
        boton_mostrar.setOnClickListener(this);

    }

    public void cargar_recicler(String cad){
        listarArtistas = new ArrayList<Artista>();
        String[] listaA1;
        String[] listaA = cad.split(";");

        for (int i=0; i<listaA.length; i++){
            Artista a = new Artista();
            listaA1= listaA[i].split(",");
            a.setNombres(listaA1[0]);
            a.setApellidos(listaA1[1]);
            listarArtistas.add(a);
        }
        adapter = new ArtistaAdapter(listarArtistas);
        recicler_mostrar_P.setLayoutManager(new LinearLayoutManager(this));
        recicler_mostrar_P.setAdapter(adapter);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnMP_mostrar:

                try{
                    InputStream input = getResources().openRawResource(R.raw.archivo_raw);

                    BufferedReader lector = new BufferedReader(new InputStreamReader(input));

                    String cadena = lector.readLine();
                    mostrar.setText(cadena);
                    cargar_recicler(cadena);
                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error de escritura"+ex.getMessage());
                }
                break;

        }
    }
}
