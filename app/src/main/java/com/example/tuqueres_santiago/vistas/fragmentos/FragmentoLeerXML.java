package com.example.tuqueres_santiago.vistas.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Godo;
import com.example.tuqueres_santiago.vistas.adapter.GodoAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoLeerXML.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoLeerXML#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoLeerXML extends Fragment implements View.OnClickListener {
    Button boton_mostrar;
    TextView mostrar;
    RecyclerView recicler_mostrar_P;

    GodoAdapter adapter;
    List<Godo> listarArtistas;
    List<Godo> lista_final;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoLeerXML() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoLeerXML.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoLeerXML newInstance(String param1, String param2) {
        FragmentoLeerXML fragment = new FragmentoLeerXML();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista =  inflater.inflate(R.layout.fragment_fragmento_leer_xml, container, false);
        recicler_mostrar_P = vista.findViewById(R.id.recycler_lista);
        boton_mostrar=vista.findViewById(R.id.btn_xml);
        mostrar = vista.findViewById(R.id.lbl_mostrar);
        boton_mostrar.setOnClickListener(this);
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public  void onbtener(){
        try{
            InputStream input = getResources().openRawResource(R.raw.reyes);
            BufferedReader lect = new BufferedReader(new InputStreamReader(input));
            String datos = lect.readLine();
            Log.e("p",datos);


        }catch (Exception ex){

        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_xml:
                try{
                    lista_final = new ArrayList<Godo>();
                    // Godos g2 = new Godos();
                    //g2.setNombre("sasa");
                    //g2.setPeriodo("sa");
                    //lista_final.add(g2);
                    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    Document doc = builder.parse(getResources().openRawResource(R.raw.reyes),null);
                    NodeList godos = doc.getElementsByTagName("godo");
                    Toast.makeText(getContext(),"numero de reyes es "+godos.getLength(),Toast.LENGTH_SHORT).show();
                    mostrar.setText("numero "+ godos.getLength());
                    for (int i=0; i < godos.getLength();i++){
                        Godo g = new Godo();

                        g.setNombre(godos.item(i).getTextContent());
                        //g.setPeriodo(godos.item(i).getTextContent());
                        lista_final.add(g);


                    }

                    adapter = new GodoAdapter(lista_final);
                    recicler_mostrar_P.setLayoutManager(new LinearLayoutManager(getContext()));
                    recicler_mostrar_P.setAdapter(adapter);


                }catch (Exception ex){

                }

                break;


        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
