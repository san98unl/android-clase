package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;

public class ActividadSensorProximidad extends AppCompatActivity implements SensorEventListener{
    Button cambiar;
    TextView x,y,z;

    SensorManager manager;
    Sensor sensor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_proximidad);

        manager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensor=manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        cargar();

    }

    public void cargar(){
        x=findViewById(R.id.lbl_x_proximidad);
        y=findViewById(R.id.lbl_y_proximidad);
        z=findViewById(R.id.lbl_z_proximidad);


    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener((SensorEventListener) this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener((SensorEventListener) this,sensor,manager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float a;
        a=event.values[0];
        //b=event.values[1];
        //c=event.values[2];

        x.setText(a+"");

        if(a>0){
            LinearLayout rl = (LinearLayout)findViewById(R.id.layout_cambiar_color);
            rl.setBackgroundColor(Color.RED);

        }else{
            LinearLayout rl = (LinearLayout)findViewById(R.id.layout_cambiar_color);
            rl.setBackgroundColor(Color.BLUE);
        }

        //y.setText(b+"");
        //z.setText(c+"");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
