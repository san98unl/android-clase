package com.example.tuqueres_santiago.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Godo;

import java.util.List;

public class GodoAdapter extends RecyclerView.Adapter<GodoAdapter.ViewHolderGodo> {
    final List<Godo> lista;

    public GodoAdapter(List<Godo> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolderGodo onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_godo,null);

        return new ViewHolderGodo(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderGodo holder, int position) {
        holder.datoNombres.setText(lista.get(position).getNombre());
        holder.datoPeriodo.setText(lista.get(position).getPeriodo());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderGodo extends RecyclerView.ViewHolder {
        TextView datoNombres;
        TextView datoPeriodo;
        public ViewHolderGodo(@NonNull View itemView) {
            super(itemView);
            datoNombres = itemView.findViewById(R.id.lblNombre);
            datoPeriodo = itemView.findViewById(R.id.lblPeriodo);
        }
    }
}
