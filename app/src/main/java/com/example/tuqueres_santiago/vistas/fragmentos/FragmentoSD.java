package com.example.tuqueres_santiago.vistas.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoSD.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoSD#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoSD extends Fragment implements View.OnClickListener {
    Button botonGuardar;
    Button buscar_todos;
    Button botonMostrar;
    EditText caja_nombres;
    EditText caja_apellidos;
    EditText caja_nombre_artistico;
    ImageView foto;
    TextView datos;

    RecyclerView recycler;
    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> lista_final;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentoSD() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentoSD.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoSD newInstance(String param1, String param2) {
        FragmentoSD fragment = new FragmentoSD();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.fragment_fragmento__sd, container, false);
        listarArtistas = new ArrayList<Artista>();
        lista_final = new ArrayList<Artista>();
        botonMostrar = vista.findViewById(R.id.btnBuscarTodosSD);
        botonGuardar = vista.findViewById(R.id.btnGuardarSD);

        caja_nombres=vista.findViewById(R.id.txtNombresSD);
        caja_apellidos = vista.findViewById(R.id.txtApellidosSD);
        caja_nombre_artistico = vista.findViewById(R.id.txtNombreArtisticoSD);
        foto=vista.findViewById(R.id.fotoSD);

        recycler=vista.findViewById(R.id.resultado);
        datos = vista.findViewById(R.id.lbldata);
        botonGuardar.setOnClickListener(this);
        //buscar_todos.setOnClickListener(this);
        botonMostrar.setOnClickListener(this);
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {



        switch (v.getId()){
            case R.id.btnGuardarSD:
                try{
                    File ruta= Environment.getExternalStorageDirectory();
                    File file = new File(ruta.getAbsoluteFile(),"archivo.txt");
                    BufferedWriter bw=null;
                    FileWriter fw= null;
                    fw = new FileWriter(file.getAbsoluteFile(),true);
                    bw= new BufferedWriter(fw);
                    bw.write(caja_nombres.getText().toString()+","+caja_apellidos.getText().toString()
                                 +","+caja_nombre_artistico.getText().toString()+";");
                    bw.close();
                    datos.setText("guardado");

                    //obtenemos la ruta del SD

                    //OutputStreamWriter escritor = new OutputStreamWriter(new FileOutputStream(file));
                    //OutputStreamWriter escritor = new OutputStreamWriter(file);
                    //escritor.write(caja_nombres.getText().toString()+","+caja_apellidos.getText().toString()
                     //       +","+caja_nombre_artistico.getText().toString()+";");
                    //escritor.close();
                    //
                }catch (Exception ex){
                    Log.e("ERROR SD","Error de escritura"+ex.getMessage());
                }
                break;
            case  R.id.btnBuscarTodosSD:
                try{
                    File ruta= Environment.getExternalStorageDirectory();
                    File file = new File(ruta.getAbsoluteFile(),"archivo.txt");
                    BufferedReader lector = new BufferedReader(new InputStreamReader((new FileInputStream(file))));
                    datos.setText(lector.readLine());
                    lector.close();
                }catch (Exception ex){
                    Log.e("EEROR SD","Error de lectura"+ex.getMessage());
                }
                break;
            case  R.id.btnMostrar:

                break;

        }


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
