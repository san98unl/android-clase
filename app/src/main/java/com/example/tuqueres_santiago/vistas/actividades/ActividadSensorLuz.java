package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tuqueres_santiago.R;

public class ActividadSensorLuz extends AppCompatActivity implements SensorEventListener {
    TextView x,y,z;

    SensorManager manager;
    Sensor sensor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_luz);
        manager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensor=manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        cargar();
    }
    public void cargar(){
        x=findViewById(R.id.lbl_x_luz);



    }
    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener((SensorEventListener) this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener((SensorEventListener) this,sensor,manager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float a;
        a=event.values[0];
        //b=event.values[1];
        //c=event.values[2];

        x.setText(a+"");


        //y.setText(b+"");
        //z.setText(c+"");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
