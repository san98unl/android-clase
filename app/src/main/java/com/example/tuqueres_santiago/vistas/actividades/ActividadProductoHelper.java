package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.controlador.HelperProducto;
import com.example.tuqueres_santiago.modelo.Producto;
import com.example.tuqueres_santiago.vistas.adapter.ProductoAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadProductoHelper extends AppCompatActivity implements View.OnClickListener {
    EditText caja_cod;
    EditText caja_descripcion;
    EditText caja_precio;
    EditText caja_cantidad;
    ProductoAdapter adapter;
    List<Producto> lista;
    List<Producto> lista_mod;
    List<Producto> lista_parcial;
    RecyclerView recicler_producto;
    HelperProducto hp;

    Button guardar;
    Button mostrar;
    Button modificar;
    Button eliminar;
    Button buscar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_producto_helper);
        tomar_control();
    }
    public void tomar_control(){
        hp= new HelperProducto(this,"bd",null,1);
        caja_cod=findViewById(R.id.txt_codigo);
        caja_descripcion=findViewById(R.id.txt_descripcion);
        caja_precio=findViewById(R.id.txt_precio);
        caja_cantidad=findViewById(R.id.txt_cantidad);

        recicler_producto=findViewById(R.id.recy_prod);

        guardar= findViewById(R.id.btn_guardar);
        mostrar= findViewById(R.id.btn_mostrar);
        modificar= findViewById(R.id.btn_modificar);
        eliminar=findViewById(R.id.btn_eliminar);
        buscar=findViewById(R.id.btn_buscar);


        guardar.setOnClickListener(this);
        mostrar.setOnClickListener(this);
        modificar.setOnClickListener(this);
        eliminar.setOnClickListener(this);
        buscar.setOnClickListener(this);



    }
    public void cargar_datos(View v){

        int cod= lista.get(recicler_producto.getChildAdapterPosition(v)).getCodigo();
        lista_mod=hp.getProductByCode(cod);
        for (Producto produm:lista_mod){
            caja_cod.setText(""+produm.getCodigo());
            caja_descripcion.setText(produm.getDescripcion());
            caja_precio.setText(""+produm.getPrecio());
            caja_cantidad.setText(""+produm.getCantidad());
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_guardar:

                Producto p= new Producto();
                p.setCodigo(Integer.parseInt(caja_cod.getText().toString()));
                p.setDescripcion(caja_descripcion.getText().toString());
                p.setPrecio(Double.parseDouble(caja_precio.getText().toString()));
                p.setCantidad(Integer.parseInt(caja_cantidad.getText().toString()));
                hp.insertar(p);
                Toast.makeText(this,"guardado ",Toast.LENGTH_SHORT).show();



                break;
            case  R.id.btn_mostrar:
                lista= new ArrayList<Producto>();
                lista=hp.getAll();
                adapter = new ProductoAdapter(lista);
                // Log.e("e",hp.getAll().get(0).getCantidad()+"");
                recicler_producto.setLayoutManager(new LinearLayoutManager(this));

                adapter.setOnclickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cargar_datos(v);


                    }
                });
                recicler_producto.setAdapter(adapter);

                break;
            case  R.id.btn_modificar:
                Producto pr = new Producto();
                pr.setCodigo(Integer.parseInt(caja_cod.getText().toString()));
                pr.setDescripcion(caja_descripcion.getText().toString());
                pr.setPrecio(Double.parseDouble(caja_precio.getText().toString()));
                pr.setCantidad(Integer.parseInt(caja_cantidad.getText().toString()));

                hp.modificar(pr);
                Toast.makeText(this,"modificado ",Toast.LENGTH_SHORT).show();
                break;
            case  R.id.btn_eliminar:
                Producto pro = new Producto();
                pro.setCodigo(Integer.parseInt(caja_cod.getText().toString()));


                hp.eliminar_por_codigo(pro);
                Toast.makeText(this,"elimimado ",Toast.LENGTH_SHORT).show();
                break;
            case  R.id.btn_buscar:
                lista_parcial= new ArrayList<Producto>();
                lista_parcial=hp.getProductByCode(Integer.parseInt(caja_cod.getText().toString()));
                adapter = new ProductoAdapter(lista_parcial);

                Log.e("e",caja_cod.getText().toString()+"");
                recicler_producto.setLayoutManager(new LinearLayoutManager(this));
                recicler_producto.setAdapter(adapter);
                break;


        }

    }
    }

