package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Alumno;
import com.example.tuqueres_santiago.modelo.Carro;
import com.example.tuqueres_santiago.vistas.adapter.AlumnoAdapter;
import com.example.tuqueres_santiago.vistas.adapter.CarroAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;

public class ActividadSWAlumnos extends AppCompatActivity implements View.OnClickListener {
Button botnGuardar, botonModificar, botonEliminar, notonBuscarTodos, botonBuscarId;
EditText cajaId,cajaNombres, cajaDireccion;
TextView datos;
RecyclerView recicler;
AlumnoAdapter adapter;

    List<Alumno> lista;
    List<Alumno> lista_temp;
    List<Alumno> lista_parcial;
//////////Definimos las URLS del Servicio
    String host= "http://reneguaman.000webhostapp.com/";
    String insert="insertar_alumno.php";
    String get = "obtener_alumnos.php";
    String update = "actualizar_alumno.php?idalumno=";
    String delete = "borrar_alumno.php";
    String getById = "obtener_alumno_por_id.php?idalumno=";

    ServicioWeb sw;
    StringBuilder result = new StringBuilder();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_servicio_web);
        cargar_componente();
    }

    //acceder al servixio web mediante un hilo
    class ServicioWeb extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... parametros) {
            String consulta = "";
            URL url = null;
            String ruta = parametros[0];//esta es la ruta... http://00rene.../obtener_alumnos.php
            if (parametros[1].equals("1")){
                try {
                    url=new URL(ruta);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    int codigo_respuesta = connection.getResponseCode();



                    if (codigo_respuesta == HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                        consulta += lector.readLine();
                        Log.e("mensaje",consulta);
                        //datos.setText(consulta);
                        JSONObject jsonjObject = new JSONObject(consulta);
                        //String valorLlave = jsonjObject.getString("alumnos");
                        JSONArray jsonArray = jsonjObject.getJSONArray("alumnos");
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            try {
                                Alumno alumno=new Alumno();
                                JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                                alumno.setId(Integer.parseInt(jsonObjectHijo.get("idalumno").toString()));
                                alumno.setNombre(jsonObjectHijo.get("nombre").toString());
                                alumno.setDireccion(jsonObjectHijo.get("direccion").toString());

                                lista.add(alumno);

                                //Log.e("u",jsonObjectHijo.get("idalumno")+"");
                            } catch (JSONException e) {
                                Log.e("Parser JSON", e.toString());
                            }
                        }


                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if (parametros[1].equals("2")){
                //vamos a establecer aqui

                try {
                    url = new URL(ruta);
                    //se puede utilizar UrlConection y HttpURLConnection para la conexion
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();
                    JSONObject json = new JSONObject();
                    //para enviar datos se usa el metodo put
                    //para obtener los datos se usa la opcion Parametoros
                    json.put("nombre", parametros[2]);
                    json.put("direccion", parametros[3]);
                    //buffer establecer la via de comunicacion
                    OutputStream os = conexion.getOutputStream();
                    BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    Log.e("daots", "guardo: " + json.toString());
                    //ENvio de datos
                    escritor.write(json.toString());
                    escritor.flush();
                    escritor.close();
                    int codigoRespuesta = conexion.getResponseCode();
                    if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                        BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                        consulta += lector.readLine();
//                        Toast.makeText(ActividadSWAlumnos.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();

                    }



                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(parametros[1].equals("3")){
                try {
                    url=new URL(ruta);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    int codigo_respuesta = connection.getResponseCode();

                    if (codigo_respuesta == HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                        consulta += lector.readLine();


                        JSONObject jsonjObject = new JSONObject(consulta);
                        //String valorLlave = jsonjObject.getString("alumnos");





                        //JSONArray jsonArray = (JSONArray) jsonjObject.getJSONArray("alumno");

                        JSONObject jObject2 = jsonjObject.getJSONObject("alumno");
                        Alumno alumno2=new Alumno();
                        alumno2.setId(Integer.parseInt(jObject2.getString("idAlumno")));
                        alumno2.setNombre(jObject2.getString("nombre"));
                        alumno2.setDireccion(jObject2.getString("direccion"));
                        lista_parcial.add(alumno2);




                        Log.e("m",jsonjObject+"");
                        Log.e("mensaje",jObject2.getString("idAlumno"));
                    }





                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else if (parametros[1].equals("4")){


                try {
                    url = new URL(ruta);
                    //se puede utilizar UrlConection y HttpURLConnection para la conexion
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();

                    JSONObject json = new JSONObject();
                    //para enviar datos se usa el metodo put
                    //para obtener los datos se usa la opcion Parametoros
                    json.put("idalumno", parametros[2]);
                    json.put("nombre", parametros[4]);
                    json.put("direccion", parametros[3]);
                    //buffer establecer la via de comunicacion
                    OutputStream os = conexion.getOutputStream();
                    BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    Log.e("daots", "guardo: " + json.toString());

                    escritor.write(json.toString());
                    escritor.flush();
                    escritor.close();

                    int codigoRespuesta = conexion.getResponseCode();
                    Log.e("sa",""+codigoRespuesta);
                    if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                        BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                        consulta += lector.readLine();
                        Log.e("daots", "guardo: " + consulta);
                       // Toast.makeText(ActividadSWAlumnos.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();

                    }else{
                        //Toast.makeText(ActividadSWAlumnos.this, "Hubo un error al guardar", Toast.LENGTH_SHORT).show();
                        Log.e("daots", "eerprs: " + consulta);
                    }



                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (parametros[1].equals("5")){
                try {
                    url = new URL(ruta);
                    //se puede utilizar UrlConection y HttpURLConnection para la conexion
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();

                    JSONObject json = new JSONObject();
                    //para enviar datos se usa el metodo put
                    //para obtener los datos se usa la opcion Parametoros
                    json.put("idalumno", parametros[2]);

                    //buffer establecer la via de comunicacion
                    OutputStream os = conexion.getOutputStream();
                    BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    Log.e("daots", "guardo: " + json.toString());

                    escritor.write(json.toString());
                    escritor.flush();
                    escritor.close();

                    int codigoRespuesta = conexion.getResponseCode();
                    Log.e("sa",""+codigoRespuesta);
                    if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                        BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                        consulta += lector.readLine();
                        Log.e("daots", "eliminado: " + consulta);
                        // Toast.makeText(ActividadSWAlumnos.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();

                    }else{
                        //Toast.makeText(ActividadSWAlumnos.this, "Hubo un error al guardar", Toast.LENGTH_SHORT).show();
                        Log.e("daots", "eerprs: " + consulta);
                    }




            } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }
            return consulta;}
    }




    public void cargar_componente(){
        cajaId=findViewById(R.id.txt_sw_id);
        cajaNombres=findViewById(R.id.txt_sw_nombre);
        cajaDireccion=findViewById(R.id.txt_sw_direccion);
        datos=findViewById(R.id.lbl_datos_sw);

        botnGuardar=findViewById(R.id.btn_sw_guardar_hilo);
        botonModificar=findViewById(R.id.btn_actualizar_hilo);
        botonEliminar=findViewById(R.id.btn_eliminar_hilo);
        notonBuscarTodos=findViewById(R.id.btn_buscar_todos_hilo);
        botonBuscarId=findViewById(R.id.btn_buscar_por_id);

        recicler=findViewById(R.id.recicler_sw);
lista= new ArrayList<Alumno>();
lista_parcial=new ArrayList<Alumno>();
        botnGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonBuscarId.setOnClickListener(this);
        notonBuscarTodos.setOnClickListener(this);


    }

    protected void onPostExecute(String s){
        datos.setText(s);
}
    @Override
    public void onClick(View v) {
        sw = new ServicioWeb();
        switch (v.getId()) {
            case R.id.btn_buscar_todos_hilo:

                sw.execute(host.concat(get),"1");//ejecuta el hilo en el Do inBackground

                adapter = new AlumnoAdapter(lista);

                //Log.e("e",lista.get(0).getNombre()+"");
                recicler.setLayoutManager(new LinearLayoutManager(this));


                recicler.setAdapter(adapter);
                break;
            case R.id.btn_sw_guardar_hilo:
                sw.execute(host.concat(insert),"2",cajaNombres.getText().toString(),cajaDireccion.getText().toString());
                Toast.makeText(this, "guardadoSW ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_buscar_por_id:
               sw.execute(host.concat(getById)+cajaId.getText().toString(),"3");
                //sw.execute(host.concat(get),"1");//ejecuta el hilo en el Do inBackground
               Toast.makeText(this, " "+host.concat(getById)+cajaId.getText().toString(), Toast.LENGTH_SHORT).show();
                adapter = new AlumnoAdapter(lista_parcial);

                //Log.e("e",lista.get(0).getNombre()+"");
                recicler.setLayoutManager(new LinearLayoutManager(this));


                recicler.setAdapter(adapter);
               break;
            case R.id.btn_actualizar_hilo:
                sw.execute(host.concat(update),"4",cajaId.getText().toString(),cajaDireccion.getText().toString(),cajaNombres.getText().toString());
                Log.e("m",""+host.concat(update)+cajaId.getText().toString()+"");
                //Toast.makeText(this, ""+host.concat(update)+cajaId.getText().toString(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_eliminar_hilo:
                sw.execute(host.concat(delete),"5",cajaId.getText().toString());
                Log.e("m","deleyed");

                break;

        }
    }

}
