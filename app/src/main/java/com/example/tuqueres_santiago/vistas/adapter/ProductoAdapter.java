package com.example.tuqueres_santiago.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Producto;

import java.util.List;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolderProducto> implements View.OnClickListener {
    final List<Producto> lista;

    private View.OnClickListener clickProducto;

    public ProductoAdapter(List<Producto> lista) {
        this.lista = lista;
    }

    @Override
    public void onClick(View v) {
        if (clickProducto!=null){
            clickProducto.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener listener){
        this.clickProducto=listener;
    }

    @NonNull
    @Override
    public ViewHolderProducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto,null);
        view.setOnClickListener(this);
        return new ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderProducto holder, int position) {
        holder.datoCodigo.setText(lista.get(position).getCodigo()+"");
        holder.datoDescripcion.setText(lista.get(position).getDescripcion()+"");
        holder.datoPrecio.setText(lista.get(position).getPrecio()+"");
        holder.datoCantidad.setText(lista.get(position).getCantidad()+"");
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderProducto extends RecyclerView.ViewHolder {
        TextView datoCodigo;
        TextView datoDescripcion;
        TextView datoPrecio;
        TextView datoCantidad;
        public ViewHolderProducto(@NonNull View itemView) {
            super(itemView);
            datoCodigo = itemView.findViewById(R.id.lblCodigo);
            datoDescripcion = itemView.findViewById(R.id.lblDescripcion);
            datoPrecio = itemView.findViewById(R.id.lblPrecio);
            datoCantidad = itemView.findViewById(R.id.lblCantidad);
        }
    }
}
