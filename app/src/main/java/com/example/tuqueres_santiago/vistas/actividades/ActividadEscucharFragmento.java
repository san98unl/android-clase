package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.vistas.fragmentos.Frag1;
import com.example.tuqueres_santiago.vistas.fragmentos.Frag2;


public class ActividadEscucharFragmento extends AppCompatActivity implements View.OnClickListener, Frag1.OnFragmentInteractionListener, Frag2.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_escuchar_fragmento);
        cargarFragmentos();

    }

    public void cargarFragmentos(){
        Frag1 fr1 = new Frag1();
        FragmentTransaction trans1 = getSupportFragmentManager().beginTransaction();
        trans1.replace(R.id.contenedor1,fr1);
        trans1.commit();

        Frag2 fr2 = new Frag2();
        FragmentTransaction trans2 = getSupportFragmentManager().beginTransaction();
        trans2.replace(R.id.contenedor2,fr2);
        trans2.commit();


    }
public void responder(String datos){
    FragmentManager fragmentManager = getSupportFragmentManager();
    Frag2 frgr1 = (Frag2) fragmentManager.findFragmentById(R.id.fragment2);
    frgr1.cambiartexto(datos);
}


    @Override
    public void onClick(View v) {

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
