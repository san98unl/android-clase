package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.MainActivity;
import com.example.tuqueres_santiago.R;

public class ActividadSensores extends AppCompatActivity implements View.OnClickListener, SensorEventListener {
Button cambiar;
TextView x,y,z;

SensorManager manager;
Sensor sensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensores);
        //Declarar qye tipo de sevicio
        manager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensor=manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);



        cargar();
        Toast.makeText(ActividadSensores.this, "Se creo", Toast.LENGTH_SHORT).show();
    }
public void cargar(){
        x=findViewById(R.id.lbl_x_acelerometro);
        y=findViewById(R.id.lbl_y_acelerometro);
        z=findViewById(R.id.lbl_z_acelerometro);

        cambiar=findViewById(R.id.btn_cambiar);
        cambiar.setOnClickListener(this);
}
    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(ActividadSensores.this, "Se pauso", Toast.LENGTH_SHORT).show();
        manager.unregisterListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(ActividadSensores.this, "Se inicio", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(ActividadSensores.this, "Se mosyro la interfaz", Toast.LENGTH_SHORT).show();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(ActividadSensores.this, "Se cerro", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.btn_cambiar:
            intent=new Intent(ActividadSensores.this, ActividadLogin.class);
            startActivity(intent);
            break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
    float a,b,c;
    a=event.values[0];
    b=event.values[1];
    c=event.values[2];

    x.setText(a+"");
    y.setText(b+"");
    z.setText(c+"");

    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//Define precision en cambios
    }
}
