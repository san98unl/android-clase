package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tuqueres_santiago.R;

public class ActividadEnviarParametro extends AppCompatActivity implements View.OnClickListener {
    EditText parametro_nombre,parametro_apellido;
    Button boton_enviar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_enviar_parametro);
        cargarComponentes();
    }
    private void cargarComponentes(){
        parametro_nombre = findViewById(R.id.txt_nombreEnviarParametro);
        parametro_apellido = findViewById(R.id.txt_apellidoEnviarParametro);
        boton_enviar=findViewById(R.id.btn_enviarParametro);
        boton_enviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //Intent intent = new Intent(ActividadEnviarParametro.this, ActividadRecibirParametros.class);
        //intent.putExtra("n1", parametro_nombre.getText().toString());
        //intent.putExtra("n2", parametro_apellido.getText().toString());
        //startActivity(intent);
        ////////////////////////////////////////////////////////////////
        Intent intent1 = new Intent(ActividadEnviarParametro.this, ActividadRecibirParametros.class);
        Bundle bundle = new Bundle();
        bundle.putString("nombres",parametro_nombre.getText().toString());
        bundle.putString("apellidos",parametro_apellido.getText().toString());
        intent1.putExtras(bundle);
        startActivity(intent1);
    }
}
