package com.example.tuqueres_santiago.vistas.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragmento_MI.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Fragmento_MI#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragmento_MI extends Fragment implements View.OnClickListener {
    Button botonGuardar;
    Button buscar_todos;
    Button botonMostrar;
    EditText caja_nombres;
    EditText caja_apellidos;
    EditText caja_nombre_artistico;
    TextView datos;
    RecyclerView recycler;
    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> lista_final;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Fragmento_MI() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragmento_MI.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragmento_MI newInstance(String param1, String param2) {
        Fragmento_MI fragment = new Fragmento_MI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.fragment_fragmento__mi, container, false);
        //tomando control de los componenetes
        listarArtistas = new ArrayList<Artista>();
        lista_final = new ArrayList<Artista>();
        botonMostrar = vista.findViewById(R.id.btnMostrar);
        botonGuardar = vista.findViewById(R.id.btnGuardarMI);
        buscar_todos= vista.findViewById(R.id.btnBuscarTodos);
        caja_nombres=vista.findViewById(R.id.txtNombresMI);
        caja_apellidos = vista.findViewById(R.id.txtApellidosMI);
        caja_nombre_artistico = vista.findViewById(R.id.txtNombreArtistico);
        recycler=vista.findViewById(R.id.resultado);
       // datos = vista.findViewById(R.id.lbldata);
        botonGuardar.setOnClickListener(this);
        buscar_todos.setOnClickListener(this);
        botonMostrar.setOnClickListener(this);
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {



        switch (v.getId()){
            case R.id.btnGuardarMI:
                try{
                    Artista artista1 = new Artista();
                    OutputStreamWriter escritor = new OutputStreamWriter(getContext().openFileOutput("archivo.txt", Context.MODE_APPEND));


                    artista1.setNombres(caja_nombres.getText().toString());
                    artista1.setApellidos(caja_apellidos.getText().toString());
                    artista1.setNombre_artistico(caja_nombre_artistico.getText().toString());
                    artista1.setFoto(R.drawable.descarga);
                    listarArtistas.add(artista1);
                    escritor.write(listarArtistas.get(0).getNombres()+","+listarArtistas.get(0).getApellidos()+","+listarArtistas.get(0).getNombre_artistico()+";");
                    //datos.setText("guardado");
                    // adapter = new ArtistaAdapter(listarArtistas);
                    //recy.setLayoutManager(new LinearLayoutManager(this));
                    //recycler.setAdapter(adapter);
                    Toast.makeText(getContext(), "guardado ", Toast.LENGTH_SHORT).show();
                    escritor.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error de escritura"+ex.getMessage());
                }
                break;
            case  R.id.btnBuscarTodos:
                try{
                    BufferedReader lector = new BufferedReader(new InputStreamReader(getContext().openFileInput("archivo.txt")));
                    String lineas = lector.readLine();
                    //datos.setText(lineas);
                    Toast.makeText(getContext(), ""+lineas, Toast.LENGTH_SHORT).show();
                    Log.e("prueba",lineas);
                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error de lectura"+ex.getMessage());
                }
                break;
            case  R.id.btnMostrar:
                adapter = new ArtistaAdapter(listarArtistas);
                recycler.setLayoutManager(new LinearLayoutManager(getContext()));
                recycler.setAdapter(adapter);
                break;

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        boolean OnCreateOptionsMenu(Menu menu, MenuInflater inflater);

        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
