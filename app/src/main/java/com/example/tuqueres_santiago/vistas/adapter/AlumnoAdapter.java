package com.example.tuqueres_santiago.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Alumno;

import java.util.List;

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolderAlumno> {
    final List<Alumno> lista;

    public AlumnoAdapter(List<Alumno> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolderAlumno onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alumno,null);

        return new ViewHolderAlumno(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderAlumno holder, int position) {
        holder.datoId.setText(lista.get(position).getId()+"");
        holder.datoNombre.setText(lista.get(position).getNombre()+"");
        holder.datoDireccion.setText(lista.get(position).getDireccion()+"");

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderAlumno extends RecyclerView.ViewHolder {
        TextView datoId;
        TextView datoNombre;
        TextView datoDireccion;

        public ViewHolderAlumno(@NonNull View itemView) {
            super(itemView);
            datoId = itemView.findViewById(R.id.lbl_sw_id);
            datoNombre = itemView.findViewById(R.id.lbl_sw_nombre);
            datoDireccion = itemView.findViewById(R.id.lbl_sw_direccion);
        }
    }
}
