package com.example.tuqueres_santiago.vistas.actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.tuqueres_santiago.MainActivity;
import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.vistas.fragmentos.FragmentoLeerXML;
import com.example.tuqueres_santiago.vistas.fragmentos.FragmentoPrograma;
import com.example.tuqueres_santiago.vistas.fragmentos.FragmentoSD;
import com.example.tuqueres_santiago.vistas.fragmentos.Fragmento_MI;
import com.example.tuqueres_santiago.vistas.fragmentos.FrgUno;

public class ActividadArchivos extends AppCompatActivity implements View.OnClickListener,Fragmento_MI.OnFragmentInteractionListener, FragmentoPrograma.OnFragmentInteractionListener, FragmentoSD.OnFragmentInteractionListener, FragmentoLeerXML.OnFragmentInteractionListener {
MenuItem it1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_archivos);
        it1=findViewById(R.id.opcionArchivos);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //metodo para cargar menus
        //Menu Inflarter permite crear un objeto para manejar el archivo xml
        //en nuestro caso es (main.xml)
        //el metodo inflate permite agrtegar un menu implementado de un archivo xml a la actividad
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.menu_mi, menu);
        inflaterMenu.inflate(R.menu.menu_programa, menu);
        inflaterMenu.inflate(R.menu.menu_sd, menu);
        inflaterMenu.inflate(R.menu.menu_xml, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //este metodo permite realizar eventos en cada item hijo de los menus
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionMI:
                Fragmento_MI fragmento1 = new Fragmento_MI();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.contenedor_archivos,fragmento1);
                transaccion1.commit();
                break;
            case R.id.opcionPrograma:
                FragmentoPrograma fragmento2 = new FragmentoPrograma();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.contenedor_archivos,fragmento2);
                transaccion2.commit();
                break;
            case R.id.opcionSD:
                FragmentoSD fragmento3 = new FragmentoSD();
                FragmentTransaction transaccion3 = getSupportFragmentManager().beginTransaction();
                transaccion3.replace(R.id.contenedor_archivos,fragmento3);
                transaccion3.commit();
                break;
            case R.id.opcionXML:
                FragmentoLeerXML fragmento4 = new FragmentoLeerXML();
                FragmentTransaction transaccion4 = getSupportFragmentManager().beginTransaction();
                transaccion4.replace(R.id.contenedor_archivos,fragmento4);
                transaccion4.commit();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {

    }

    @Override
    public boolean OnCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
