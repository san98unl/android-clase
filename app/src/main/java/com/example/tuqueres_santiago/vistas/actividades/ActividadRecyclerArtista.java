package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;
import com.example.tuqueres_santiago.vistas.adapter.ArtistaAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActividadRecyclerArtista extends AppCompatActivity {

    RecyclerView recyclerViewArtistas;
    ArtistaAdapter adapter;
    List<Artista> listarArtistas;
    List<Artista> listarArtistas_env;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recycler_artista);
        tomar_control();
        cargarRecycler();
    }

    private void tomar_control(){
        recyclerViewArtistas = findViewById(R.id.recicler_artista);
    }
    private void cargarRecycler()  {
        listarArtistas = new ArrayList<Artista>();
        Artista artista1 = new Artista();
        artista1.setNombres("Maria");
        artista1.setApellidos("Rojas");

        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();

        artista1.setFechaNacimiento(todayDate);
        artista1.setFoto(R.drawable.descarga);
        listarArtistas.add(artista1);

        ////////////////////////////////7
        Artista artista2 = new Artista();
        artista2.setNombres("Luis Miguel");
        artista2.setApellidos("Rojas");


        SimpleDateFormat currentDate1 = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate1 = new Date();
        artista2.setFechaNacimiento(todayDate1);

        artista2.setFoto(R.drawable.images);
        listarArtistas.add(artista2);
        /////////////////////////////////////7

        //Artista a = new Artista();
        //a.setLista(listarArtistas);

        artista1.setLista(listarArtistas);

        adapter = new ArtistaAdapter(listarArtistas);
        recyclerViewArtistas.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewArtistas.setAdapter(adapter);




    }
}
