package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.controlador.ServicioWebVolleyAlumnos;
import com.example.tuqueres_santiago.modelo.Alumno;
import com.example.tuqueres_santiago.modelo.config.AlumnoSW;
import com.example.tuqueres_santiago.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadSWVolleyAlumno extends AppCompatActivity implements View.OnClickListener {
    TextView dat;
    EditText caja_nombre;
    EditText caja_direccion;
    EditText caja_id;
    Button save;
    Button update;
    Button delete;
    Button FindAll;
    Button FindId;
    RecyclerView recicler;
    AlumnoAdapter adapter;

    List<Alumno> lista;
ServicioWebVolleyAlumnos swa = new ServicioWebVolleyAlumnos(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swvolley_alumno);
        dat=findViewById(R.id.lb_v);
        caja_nombre=findViewById(R.id.txt_sw_nombre_volley);
        caja_direccion=findViewById(R.id.txt_sw_direccion_volley);
        caja_id=findViewById(R.id.txt_sw_id_volley);
        save=findViewById(R.id.btn_sw_guardar_vollet);
        delete=findViewById(R.id.btn_sw_delete_vollet);
        update=findViewById(R.id.btn_sw_update_vollet);
        FindAll=findViewById(R.id.btn_sw_search_all_vollet);
        FindId=findViewById(R.id.btn_sw_search_id_vollet);
        update.setOnClickListener(this);
        save.setOnClickListener(this);
        delete.setOnClickListener(this);
        FindId.setOnClickListener(this);
        FindAll.setOnClickListener(this);
        lista=new ArrayList<Alumno>();
        //AlumnoSingletonVolley crea un solo objeto


    }
    public void obtener(){

            dat.setText(swa.findAllAlumnos());
       // dat.setText(swa.allStudents());





    }
    public void insertar(){

        AlumnoSW s= new AlumnoSW();
        s.setNombres(caja_nombre.getText().toString());
        s.setDieccion(caja_direccion.getText().toString());
        swa.insertarStudents(s);
        //dat.setText(swa.insertarStudents(s)+"");
        Toast.makeText(this,"guardado",Toast.LENGTH_SHORT).show();
    }
    public void modificar(){
        AlumnoSW s= new AlumnoSW();
        s.setId(Integer.parseInt(caja_id.getText().toString()));
        s.setNombres(caja_nombre.getText().toString());
        s.setDieccion(caja_direccion.getText().toString());
        swa.modificar_estudiante(s);
        //dat.setText(swa.modificar_estudiante(s)+"");
        Toast.makeText(this,"modificado",Toast.LENGTH_SHORT).show();
    }
    public void eliminar(){
        AlumnoSW s= new AlumnoSW();
        s.setId(Integer.parseInt(caja_id.getText().toString()));

        swa.eliminarEstudiante(s);
        //dat.setText(swa.eliminarEstudiante(s)+"");
        Toast.makeText(this,"eliminado",Toast.LENGTH_SHORT).show();
    }
    public void obtenerPorId(){
        AlumnoSW s= new AlumnoSW();
        s.setId(Integer.parseInt(caja_id.getText().toString()));

        //swa.findByIdAlumnos(caja_id.getText().toString());
        dat.setText(swa.findByIdAlumnos(caja_id.getText().toString())+"");
    }

    @Override
    public void onClick(View v) {
       // sw = new ServicioWeb();
        switch (v.getId()) {
            case R.id.btn_sw_guardar_vollet:

               insertar();
                Log.e("guardar","guardado con exito");
                break;
            case R.id.btn_sw_update_vollet:

                modificar();
                Log.e("modificado","modificado con exito");
                break;
            case R.id.btn_sw_delete_vollet:

                eliminar();
                Log.e("eliminado","eliminado con exito");
                break;
            case R.id.btn_sw_search_all_vollet:

                obtener();


                //Log.e("obtner","obtner");
                break;
            case R.id.btn_sw_search_id_vollet:

                obtenerPorId();
                Log.e("obtner","obtner x id");
                break;



        }
    }
}
