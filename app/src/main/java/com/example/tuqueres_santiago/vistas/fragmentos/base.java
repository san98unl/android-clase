package com.example.tuqueres_santiago.vistas.fragmentos;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.tuqueres_santiago.R;

public class base extends AppCompatActivity implements View.OnClickListener, Frag1.OnFragmentInteractionListener,Frag2.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        cargarFragmentos();
    }
    public void cargarFragmentos(){



    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
