package com.example.tuqueres_santiago.vistas.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tuqueres_santiago.MainActivity;
import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.modelo.Artista;

import java.util.List;

public class ArtistaAdapter extends RecyclerView.Adapter<ArtistaAdapter.ViewHolderArtista> {
 final List<Artista> lista;

 public  ArtistaAdapter(List<Artista> lista){
     this.lista=lista;
     return;
 }
    public List<Artista> ArtistaAdapter(List<Artista> lista){
        Artista artista = new Artista();

        lista=artista.getLista();
        return lista;
    }
    @NonNull
    @Override
    public ViewHolderArtista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artista,null);

        return new ViewHolderArtista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderArtista holder, int position) {
        holder.datoNombres.setText(lista.get(position).getNombres());
        holder.datoApellidos.setText(lista.get(position).getApellidos());
       // holder.datoFechaNacimiento.setText(lista.get(position).getFechaNacimiento().);

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public static class ViewHolderArtista extends RecyclerView.ViewHolder{
        TextView datoNombres;
        TextView datoApellidos;
      //  TextView datoFechaNacimiento;

        public ViewHolderArtista(@NonNull View itemView) {
            super(itemView);
            datoNombres = itemView.findViewById(R.id.lblNombresArtista);
            datoApellidos = itemView.findViewById(R.id.lblApellidosArtista);
           // datoFechaNacimiento = itemView.findViewById(R.id.date);
            datoNombres.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Toast.makeText(v.getContext(),"yyy",Toast.LENGTH_SHORT).show();
                    //c3.setText(datoFechaNacimiento.getText());


                }
            });


        }
    }


}
