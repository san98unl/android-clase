package com.example.tuqueres_santiago.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;
import com.example.tuqueres_santiago.controlador_deber.ServicioWebUsuario;
import com.example.tuqueres_santiago.modelo.Usuario;
import com.example.tuqueres_santiago.vistas.adapter.UsuarioAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DeberVolley extends AppCompatActivity implements View.OnClickListener {
    ServicioWebUsuario swa = new ServicioWebUsuario(this);

    Button boton_listar,boton_guardar,boton_update,boton_buscar_documento,boton_eliminar;
    EditText caja_documento,caja_nombre,caja_profesion;

    RecyclerView recicler;
    UsuarioAdapter adapter;
    List<Usuario> lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deber_volley);
 cargar_componentes();
    }
    private void cargar(List<Usuario> estudiantes){

        adapter = new UsuarioAdapter(estudiantes);
        recicler.setLayoutManager(new LinearLayoutManager(this));

        recicler.setAdapter(adapter);
    }

    public void cargar_componentes(){
        caja_documento=findViewById(R.id.txt_volley_documento);
        caja_nombre=findViewById(R.id.txt_volley_nombre);
        caja_profesion=findViewById(R.id.txt_volley_profesion);
        boton_listar=findViewById(R.id.btn_listar_volley);
        boton_guardar=findViewById(R.id.btn_guardar_volley);
        boton_update=findViewById(R.id.btn_update_volley);
        recicler=findViewById(R.id.rec_volley);
        boton_buscar_documento=findViewById(R.id.btn_buscar_documento_volley);
        boton_eliminar=findViewById(R.id.btn_delete_volley);
        boton_eliminar.setOnClickListener(this);
        boton_guardar.setOnClickListener(this);
        boton_update.setOnClickListener(this);
        boton_listar.setOnClickListener(this);
        boton_buscar_documento.setOnClickListener(this);


    }
    public void eliminar(){
        Usuario s= new Usuario();
        s.setDocumento(Integer.parseInt(caja_documento.getText().toString()));

        swa.eliminarUsario(s);
        //dat.setText(swa.eliminarEstudiante(s)+"");
        Toast.makeText(this,"eliminado",Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_listar_volley:
                lista=new ArrayList<Usuario>();
                try {
                    swa.findAll(new ServicioWebUsuario.VolleyResponseListener(){

                        @Override
                        public void onError(String message) {

                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray jsonArray = null;
                            try {
                                jsonArray = response.getJSONArray("usuario");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // JSONObject jObject2 = jsonjObject.getJSONObject("coord");

                            // Log.e("mensaje1", jObject2.toString());
                            Log.e("mensaje", jsonArray+"");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                try {
                                    Usuario alumno=new Usuario();
                                    JSONObject jsonObjectHijo = jsonArray.getJSONObject(i);
                                    alumno.setDocumento(Integer.parseInt(jsonObjectHijo.get("documento").toString()));
                                    alumno.setNombre(jsonObjectHijo.get("nombre").toString());
                                    alumno.setProfesion(jsonObjectHijo.get("profesion").toString());

                                    lista.add(alumno);


                                    //Log.e("u",jsonObjectHijo.get("idalumno")+"");
                                } catch (JSONException e) {
                                    Log.e("Parser JSON", e.toString());
                                }
                            }
                            adapter = new UsuarioAdapter(lista);
                            recicler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                            recicler.setAdapter(adapter);

                        }
                    });
                }catch (Exception ex){
                    Log.e("error",ex+"");
                }

                break;
            case R.id.btn_guardar_volley:
                try {
                    Usuario user = new Usuario();
                    user.setDocumento(Integer.parseInt(caja_documento.getText().toString()));
                    user.setNombre(caja_nombre.getText().toString());
                    user.setProfesion(caja_profesion.getText().toString());
                    //swa.insertarUser(user);
                    swa.insertarUserPrueba2(user);
                    Toast.makeText(DeberVolley.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error",ex+"");
                }

                break;
            case R.id.btn_update_volley:
                Usuario user = new Usuario();
                user.setDocumento(Integer.parseInt(caja_documento.getText().toString()));
                user.setNombre(caja_nombre.getText().toString());
                user.setProfesion(caja_profesion.getText().toString());
                swa.updateUser(user);
                Toast.makeText(DeberVolley.this, "Actualizado", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btn_delete_volley:
            eliminar();
                Toast.makeText(DeberVolley.this, "Eliminado Exitosomente", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btn_buscar_documento_volley:
                try {

                    swa.findByDoc(caja_documento.getText().toString());
                    //Toast.makeText(DeberVolley.this, "Se Guardo Exitosomente", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error",ex+"");
                }
                break;



        }
    }
}
