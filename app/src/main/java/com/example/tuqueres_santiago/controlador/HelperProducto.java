package com.example.tuqueres_santiago.controlador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.tuqueres_santiago.modelo.Producto;

import java.util.ArrayList;
import java.util.List;

public class HelperProducto extends SQLiteOpenHelper {
    public HelperProducto(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public void insertar(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo",producto.getCodigo());
        valores.put("descripcion",producto.getDescripcion());
        valores.put("precio",producto.getPrecio());
        valores.put("cantidad",producto.getCantidad());

        this.getWritableDatabase().insert("producto",null,valores);

    }
    public void modificar(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put("codigo",producto.getCodigo());
        valores.put("descripcion",producto.getDescripcion());
        valores.put("precio",producto.getPrecio());
        valores.put("cantidad",producto.getCantidad());

        this.getWritableDatabase().update("producto",valores,"codigo="+producto.getCodigo()+"",null);

    }
    public void eliminar_por_codigo(Producto producto){

        this.getWritableDatabase().delete("producto",null,null);
    }
    public List<Producto> getProductByCode(int code){
        List<Producto>lista = new ArrayList<Producto>();
        Cursor cursor = this.getReadableDatabase().rawQuery("select* from producto where codigo="+code+"",null);
        if (cursor.moveToFirst()){
            do {
                Producto p =new Producto();
                p.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                p.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                p.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                p.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));

                lista.add(p);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }

    public List<Producto> getAll(){
        List<Producto>lista = new ArrayList<Producto>();
        //rawquery(sql,where)
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM producto",null);
        if(cursor.moveToFirst()){
            do {
                Producto p= new Producto();
                p.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                p.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                p.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                p.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));

                lista.add(p);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE producto(" +
                "id integer Primary Key AUTOINCREMENT," +
                "codigo integer," +
                "descripcion text," +
                "precio decimal(6.3)," +
                "cantidad integer"+
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
