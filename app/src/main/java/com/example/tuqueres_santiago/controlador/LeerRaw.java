package com.example.tuqueres_santiago.controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.tuqueres_santiago.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static com.activeandroid.Cache.getContext;

public class LeerRaw {
    public String leer(){
        String cadena="";
        try{
            BufferedReader lector = new BufferedReader(new InputStreamReader(getContext().getResources().openRawResource(R.raw.coordenadas)));
            String lineas = lector.readLine();
            //datos.setText(lineas);
            Toast.makeText(getContext(), ""+lineas, Toast.LENGTH_SHORT).show();
            android.util.Log.e("prueba",lineas);
            cadena=lineas;
            lector.close();
            //cadena=lineas.split(";");
            Log.e("e",cadena);

        }catch (Exception ex){
            android.util.Log.e("Archivo MI","Error de lectura"+ex.getMessage());
        }
        return cadena;
    }
}
