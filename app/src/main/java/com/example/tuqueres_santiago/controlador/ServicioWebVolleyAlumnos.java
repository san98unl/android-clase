package com.example.tuqueres_santiago.controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.tuqueres_santiago.modelo.Alumno;
import com.example.tuqueres_santiago.modelo.config.AlumnoSW;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicioWebVolleyAlumnos {
    String host= "http://reneguaman.000webhostapp.com/";
    String insert="insertar_alumno.php";
    String get = "obtener_alumnos.php";
    String update = "actualizar_alumno.php?idalumno=";
    String delete = "borrar_alumno.php";
    String getById = "obtener_alumno_por_id.php?idalumno=";

    String consulta;

    Context context;

    boolean estado;

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(Object response);
    }
    public ServicioWebVolleyAlumnos(Context context) {
        this.context = context;
    }

    public String findAllAlumnos(){
        String path = host.concat(get);
        StringRequest request =  new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                consulta=response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error","error");
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);
        return consulta;
    }
    public String findByIdAlumnos(String id){
        String path = host.concat(getById)+id;
        StringRequest request =  new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                consulta=response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error","error");
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);
        return consulta;
    }
    public JSONObject allStudents(final VolleyResponseListener listener){
        String path = host.concat(get);
        JSONObject json = new JSONObject();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response.toString());
                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","error");
                listener.onError(error.toString());
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }

    public JSONObject insertarStudents( AlumnoSW alumno){
        String path = host.concat(insert);
        JSONObject json = new JSONObject();
        try {
            json.put("nombre",alumno.getNombres());
            json.put("direccion",alumno.getDieccion());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","error");
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }
    public JSONObject modificar_estudiante(AlumnoSW alumno){
        String path = host.concat(update);
        JSONObject json = new JSONObject();
        try {
            json.put("idalumno",alumno.getId());
            json.put("nombre",alumno.getNombres());
            json.put("direccion",alumno.getDieccion());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","error");
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }
    public JSONObject eliminarEstudiante(AlumnoSW alumnoSW){
        String path = host.concat(delete);
        JSONObject json = new JSONObject();
        try {
            json.put("idalumno",alumnoSW.getId());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, json,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("res",response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","error");
            }
        });
        AlumnoSingletonVolley.getInstance(context).addToRequestque(request);

        return json;
    }

}
