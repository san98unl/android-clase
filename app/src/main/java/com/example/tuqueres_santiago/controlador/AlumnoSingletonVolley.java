package com.example.tuqueres_santiago.controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.tuqueres_santiago.modelo.config.AlumnoSW;

public class AlumnoSingletonVolley {
    //un solo objeto
 private RequestQueue queue;
 private Context context;
 private static AlumnoSingletonVolley miInstancia;

 public AlumnoSingletonVolley(Context context){
     this.context=context;
     queue = getRequestQueue();


 }
 public RequestQueue getRequestQueue(){
     if (queue == null)
     queue = Volley.newRequestQueue(context.getApplicationContext());

     return queue;
 }
 public static synchronized AlumnoSingletonVolley getInstance(Context context){
     if (miInstancia == null){
         miInstancia = new AlumnoSingletonVolley(context);
     }

return miInstancia;
 }

 public <T>  void  addToRequestque(Request request){
     queue.add(request);

 }

}
