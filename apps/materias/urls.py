from django.urls import path
from .import views


urlpatterns = [
 path('buscar/',views.busqueda, name="busqueda"),
 path('mostrar',views.mostrar, name="mostrar"),
 path('crear_materia/',views.crear_materia),
 path('listar',views.listar),
]