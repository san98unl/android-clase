from django import forms
from apps.modelo.models import Materia

from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField

class FormularioMateria(forms.ModelForm):
	class Meta:
		model = Materia
		fields=["nombre","nota1","nota2","nota3","prom","estado"]#lo que voy a representar en la parte grafica
		widgets = {
		'nombre' : forms.TextInput(attrs={'class':'form-control'}),
		'nota1' : forms.TextInput(attrs={'class':'form-control'}),
		'nota2' : forms.TextInput(attrs={'class':'form-control'}),
		'nota3' : forms.TextInput(attrs={'class':'form-control'}),
		'prom' : forms.TextInput(attrs={'class':'form-control'}),
		'estado' : forms.TextInput(attrs={'class':'form-control'}),


		
		}
