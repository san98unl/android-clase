from django.shortcuts import render, redirect
from .forms import FormularioMateria

from django.contrib.auth.decorators import login_required
from apps.modelo.models import Materia,Estudiante
#importae numeros aleatorios
from django.utils.crypto import get_random_string
import random

#Para Buscar
from django.db.models import Q
from django.shortcuts import render_to_response

# Create your views here.
def mostrar(request):
	

	return render (request,'materia/busqueda.html')
#
def busqueda(request):
    query = request.GET.get('q', '')

    print(query)
    if query:
        qset = (
            Q(cedula=query) 
        )

        results = Estudiante.objects.filter(qset).distinct()
    else:
        results = []
    return render_to_response("materia/busqueda.html", {
        "results": results,
        "query": query
    })
def crear_materia(request):
	dni = request.GET['cedula']
	estudiante = Estudiante.objects.get(cedula = dni)

	formulario = FormularioMateria(request.POST)
		
	 
		
	if request.method == 'POST':
		if formulario.is_valid():	
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				materia=Materia() #asi se crear los objetos en python
				materia.nombre=datos.get('nombre')
				materia.nota1=datos.get('nota1')
				materia.nota2=datos.get('nota2')
				materia.nota3=datos.get('nota1')
				materia.prom=datos.get('prom')
				materia.estado=datos.get('estado')


				
				materia.estudiante_id=estudiante.estudiante_id
				
				
				materia.save()
				

		return redirect(mostrar)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			

	}
	return render (request,'materia/crear_materias.html', context)
def listar(request):
	lista=Materia.objects.all()#elemnto del orm objetss.all oltengo los elementos y los almaceno en lista
	
	context ={
		'lista' : lista,
		

	}
	return render (request,'materia/listar.html',context)