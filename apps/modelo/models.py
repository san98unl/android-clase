from django.db import models

# Create your models here.
class Estudiante(models.Model):
	
	
	estudiante_id = models.AutoField(primary_key = True)#es el auto incrementable
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
class Materia(models.Model):
	
	materia_id = models.AutoField(primary_key = True)
	
	nombre = models.CharField(max_length = 50, null = False)
	nota1 = models.DecimalField(max_digits=10, decimal_places=3, null = False)
	nota2 = models.DecimalField(max_digits=10, decimal_places=3, null = False)
	nota3 = models.DecimalField(max_digits=10, decimal_places=3, null = False)
	prom = models.DecimalField(max_digits=10, decimal_places=3, null = False,default=0)
	estado = models.CharField(max_length = 50, null = False,default='')
	estudiante=models.ForeignKey('Estudiante',default=1,verbose_name="Estudiante",on_delete=models.CASCADE)
	
	#cliente = models.ForeignKey(
	#	'Cliente',#referencia al modelo
	#	on_delete=models.CASCADE,
	#	)
