from django.shortcuts import render, redirect
from .forms import FormularioEstudiante

from django.contrib.auth.decorators import login_required
from apps.modelo.models import Estudiante
#importae numeros aleatorios
from django.utils.crypto import get_random_string
import random

# Create your views here.
def principal(request):
	

	return render (request,'principal.html')
def crear(request):
	formulario = FormularioEstudiante(request.POST)
	
	
		
	if request.method == 'POST':
		if formulario.is_valid() :
			datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
			estudiante=Estudiante() #asi se crear los objetos en python
			estudiante.cedula=datos.get('cedula')
			estudiante.nombres=datos.get('nombres')
			estudiante.apellidos=datos.get('apellidos')
			
			estudiante.save()
				

		return redirect(principal)
		#else:
				#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario
				

			}
	return render (request,'estudiante/crear_estudiante.html', context)