from django import forms
from apps.modelo.models import Estudiante

from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField
class FormularioEstudiante(forms.ModelForm):
	class Meta:
		model = Estudiante
		fields=["cedula","nombres","apellidos"]#lo que voy a representar en la parte grafica
		widgets = {
		'cedula' : forms.TextInput(attrs={'class':'form-control'}),
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		
		}